<?php
class Errors extends Exception {
    private static $errorsType = array(
        'General Error',
        'Business Logic',
        'Database',
        'Primary Key Constraint',
        'Foreign Key Constraint',
        'Unique Constraint',
        'Connection',
        'Developer',
        'Dump',
        'Authorization',
        'Application Logic',
        'Router',
        'Middleware',
        'Not Found'
    );
    private static $lean;
    private static $response;

    public function __construct( $pMessage, $pCode = 0 ) {
        parent::__construct( $pMessage, $pCode, null );
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public static function destroyer() {
        self::$lean = null;
        self::$response = null;
    }

    private static function register( $pHttpStatusCode, $pErrorType, $pMessage, $pExtraValues = null, $pFile = null, $pLine = null, $pLog = true, $pIsManagedErrorByDev = null ) {
        self::$lean = \Lean::getInstance();
        self::$response = self::$lean->getResponseInstance();
        $messageOrig = \Helper::translate( $pMessage, $pExtraValues );
        $message = $messageOrig;
        $code = $pErrorType + 1;
        $type = self::$errorsType[$pErrorType];
        $file = $pFile;
        $line = $pLine;
        
        if( $pErrorType === 9 || $pErrorType === 11 ) {
            self::$lean->setIsRESTful(true);
        }

        self::withHttpStatusCode( is_null($pHttpStatusCode) ? 500 : $pHttpStatusCode );

        if( self::$lean->getDebug() === false ) {
            $file = null;
            $line = null;
            
            if( $pIsManagedErrorByDev === false && self::isManagedErrorByDev($pFile) === false ) {
                $message = 'A problem has occurred. Try again in a few minutes.';
            } 
        }

        self::$response->setError(array(
            'code' => $code,
            'type' => $type
        ), $message, $file, $line);

        if( self::$lean->getIsRESTful() && self::$response->getHttpStatusCode() === 200 ) {
            self::withHttpStatusCode(500);
        }

        if( $pLog ) {
            //Evitamos que se cicle si el problema es por mongo.
            if( !strpos( $messageOrig, 'MongoDB' ) ) {
                \Logger::error( $messageOrig, $pErrorType, $pFile, $pLine );
            }
        }

        return array(
            'message' => $message,
            'file' => $file,
            'line' => $line,
            'code' => $code,
            'type' => $type
        );
    }

    public function clear() {
        self::$response->clearError();
    }

    private static function isManagedErrorByDev( $pFile ) {
        $classArr = ['Auth.php', 'BaseController.php', 'BaseDomain.php', 'BaseMiddleware.php', 'BaseTest.php', 'Cache.php'
            , 'DBAccess.php', 'Errors.php', 'Helper.php', 'Lean.php', 'Logger.php', 'Request.php', 'Response.php', 'Router.php'];
        $fileClass = explode( '/', $pFile );
        $fileClass = $fileClass[ count($fileClass) - 1 ];

        return !in_array( $fileClass, $classArr );
    }

    public static function getErrorType( $pIdx = null ) {
        if( is_null($pIdx) ) return self::$errorsType;

        return self::$errorsType[$pIdx];
    }

    public static function withHttpStatusCode( $pHttpStatusCode ) {
        if( self::$lean->getIsCli() === false ) {
            self::$response->setHttpStatusCode( self::$lean->getIsRESTful() === true ? $pHttpStatusCode : 200 );
        }
    }

    public static function controller( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 400, 10, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function domain( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 400, 1, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function database( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 500, 2, $pMessage, $pExtraValues, $pFile, $pLine, true );
    }

    public static function databaseConnection( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 500, 6, $pMessage, $pExtraValues, $pFile, $pLine, true );
    }

    public static function auth( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 401, 9, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function middleware( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 401, 12, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function notFound( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 404, 13, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function errorsWithHttpStatusCode( $pStatusCode, $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( $pStatusCode, 9, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    //For internal use only

    public static function exception( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( null, 0, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function uncaught( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 500, 0, $pMessage, $pExtraValues, $pFile, $pLine, true, false );
    }

    public static function developer( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        return self::register( 500, 7, $pMessage, $pExtraValues, $pFile, $pLine );
    }

    public static function primaryKey( $pPK = null, $pMessage = null, $pExtraValues = null, $pFile = null, $pLine = null ) {
        $lean = \Lean::getInstance();
        $message = $pMessage;
        $byDev = false;

        if( is_null( $pPK ) || is_null( $pMessage ) ) {
            return self::developer( 'Be sure to specify the correct parameters for the "' . __FUNCTION__ . '" method.' );
        } else if( isset( $lean->getCustomMessages('pk')[ $pPK ] ) ) {
            $messageArr = $lean->getCustomMessages('pk')[ $pPK ];
            $message = $messageArr['message'];
            $byDev = true;
        }

        return self::register( 500, 3, $message, $pExtraValues, $pFile, $pLine, true, $byDev );
    }

    public static function foreignKey( $pFK = null, $pMessage = null, $pExtraValues = null, $pFile = null, $pLine = null ) {
        $lean = \Lean::getInstance();
        $message = $pMessage;
        $byDev = false;

        if( is_null( $pFK ) || is_null( $pMessage ) ) {
            return self::developer( 'Be sure to specify the correct parameters for the "' . __FUNCTION__ . '" method.' );
        } else {
            if( isset( $lean->getCustomMessages('fk')[ $pFK ] ) ) {
                $messageArr = $lean->getCustomMessages('fk')[ $pFK ];
                $message = $messageArr['message'];
                $byDev = true;
            }
        }

        return self::register( 500, 4, $message, $pExtraValues, $pFile, $pLine, true, $byDev );
    }

    public static function uniqueKey( $pUniqueC = null, $pMessage = null, $pExtraValues = null, $pFile = null, $pLine = null ) {
        $lean = \Lean::getInstance();
        $message = $pMessage;
        $byDev = false;

        if( is_null( $pUniqueC ) || is_null( $pMessage ) ) {
            return self::developer( 'Be sure to specify the correct parameters for the "' . __FUNCTION__ . '" method.' );
        } else {
            if( isset( $lean->getCustomMessages('uq')[ $pUniqueC ] ) ) {
                $messageArr = $lean->getCustomMessages('uq')[ $pUniqueC ];
                $message = $messageArr['message'];
                $byDev = true;
            } 
        }

        return self::register( 500, 5, $message, $pExtraValues, $pFile, $pLine, true, $byDev );
    }

    public static function router( $pMessage, $pExtraValues = null, $pFile = null, $pLine = null ) {
        $exception = new self( null, 11, $pMessage, $pExtraValues );

        return self::register( null, 11, $pMessage, $pExtraValues, $pFile, $pLine );
    }
}

class ErrorsWithCode extends Errors {
    public function __construct( $pStatusCode, $pMessage, $pExtraValues = null ) {
        $error = Errors::errorsWithHttpStatusCode( $pStatusCode, $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsController extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::controller( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsDomain extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::domain( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsDatabase extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::database( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsDatabaseConnection extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::databaseConnection( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsAuth extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::auth( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsMiddleware extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::middleware( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsNotFound extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::notFound( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

//For internal use only

class ErrorsException extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::exception( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsUncaught extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::uncaught( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsDeveloper extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::developer( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsPrimaryKey extends Errors {
    public function __construct( $pPK, $pMessage, $pExtraValues = null ) {
        $error = Errors::primaryKey( $pPK, $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsForeignKey extends Errors {
    public function __construct( $pFK, $pMessage, $pExtraValues = null ) {
        $error = Errors::foreignKey( $pFK, $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsUniqueKey extends Errors {
    public function __construct( $pUniqueC, $pMessage, $pExtraValues = null ) {
        $error = Errors::uniqueKey( $pUniqueC, $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}

class ErrorsRouter extends Errors {
    public function __construct( $pMessage, $pExtraValues = null ) {
        $error = Errors::router( $pMessage, $pExtraValues, $this->getFile(), $this->getLine() );
        parent::__construct( $error['message'], 0 );
    }
}