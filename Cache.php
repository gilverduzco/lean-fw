<?php
/* 
	Cache type:
		1.  API
		2.  Database
		3.  Custom
*/

class Cache {
	private static $redis = null;
	private static $verifyRedisCacheCnn = true;

	private static function setRedisConnection() {
		if( self::$verifyRedisCacheCnn && !self::$redis ) {
			$cacheCnn = \Helper::getConfig('cache');
			
			if( $cacheCnn ) {
				if( !( isset( $cacheCnn['host'] ) && isset( $cacheCnn['db'] ) ) ) {
					throw new \ErrorsDeveloper('Make sure to define the following variables: "host", "db" and "port" (port is optional).');
				}

				$host = $cacheCnn['host'];
				$port = isset( $cacheCnn['port'] ) && $cacheCnn['port'] ? $cacheCnn['port'] : 6379;
				$db = $cacheCnn['db'] ? $cacheCnn['db'] : 1;

				self::$redis = new \Redis();
				self::$redis->connect( $host, $port );
				self::$redis->select($db);
			} else self::$verifyRedisCacheCnn = false;
		}
	}

	public static function set( $pKey, $pSeconds, $pDataToCache ) {
		if( !is_string($pDataToCache) ) {
			throw new \ErrorsDeveloper('Data to cached must be a string.');
		}

		self::setRedisConnection();

		if( self::$redis ) {
			self::$redis->setEx( $pKey, $pSeconds, $pDataToCache );
		}
	}

	public static function get( $pKey ) {
		self::setRedisConnection();

		if( self::$redis ) {
			return self::$redis->get($pKey);
		}
	}

	public static function del( $pKey ) {
		self::setRedisConnection();

		if( self::$redis ) {
			return self::$redis->del($pKey);
		}
	}

	public static function remember( $pKey, $pSeconds, $pFnc ) {
		$dataCache = null;

		if( is_string($pKey) && $pKey !== '' && is_int($pSeconds) && is_callable($pFnc) ) {
			$dataCache = self::get($pKey);

			if( $dataCache ) {
				$dataCacheAux = json_decode( $dataCache, true );

				if( is_array($dataCacheAux) ) {
					return $dataCacheAux;
				}
			} else {
				$dataCache = $pFnc();

				if( is_string($dataCache) || is_array($dataCache) ) {
					self::set( $pKey, $pSeconds, (is_string($dataCache) ? $dataCache : json_encode($dataCache, JSON_UNESCAPED_UNICODE)) );
				}
			}
		}

		return $dataCache;
	}
}