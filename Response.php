<?php
class Response {
    private static $instance;
    private $lean;
    private $success;
    private $data;
    private $error;
    private $answered;
    private $httpStatusCode;

    public function __construct() {
        $this->lean = \Lean::getInstance();
        $this->initProperties();

        if( function_exists('error_clear_last')  ) error_clear_last();
    }

    public function __destruct() {
        if( $this->lean->getIsCli() === true ) {
            $this->lean = null;
            $this->initProperties();
        }
    }

    public function initProperties() {
        $this->success = true;
        $this->data = null;
        $this->error = null;
        $this->answered = false;
        $this->httpStatusCode = 200;
    }

    public function setData( $pValue ) {
        $this->data = $pValue;
    }

    public function setError( $pType, $pMessages, $pFile = null, $pLine = null ) { 
        $this->success = false;
        $this->error = new \ResponseError($pType, $pMessages, $pFile, $pLine);
    }

    public function clearError() {
        $this->success = true;
        $this->error = null;
        $this->httpStatusCode = 200;
    }

    public function getError() {
        return $this->error ? $this->error->getErrorForResponse() : null;
    }

    public function setHttpStatusCode( $pValue ) {
        $this->httpStatusCode = $pValue;

        if( $this->lean->getIsCli() === false ) {
            http_response_code($pValue);
        }
    }

    public function getHttpStatusCode() {
        return $this->httpStatusCode;
    }

    public function sanitize( $pString ) {
        $valAux = $pString;
        
        if( $this->lean->getIsCli() === true && $this->lean->getXSS() === true ) {
            $valAux = htmlspecialchars( $pString, ENT_NOQUOTES | ENT_HTML401 | ENT_DISALLOWED, 'UTF-8' );
        }

        return $valAux;
    }

    public function send( $pData ) {
        //if( $this->answered === false ) {
            $this->answered = true;
            $response = [
                'data' => $this->success ? $pData : null,
                'error' => $this->getError(),
                'success' => $this->success
            ];

            \Logger::getInstance()->log(2);
            
            if( $this->lean->getIsCli() === false ) {
                ob_clean();
                header('Content-Type: application/json; charset=utf-8');
                echo $this->sanitize( json_encode( $response, JSON_UNESCAPED_UNICODE ) );
                die();
            } else {
                if( $this->lean->getUnitTestsMode() === true ) {
                    if( $this->success === false ) {
                        echo json_encode( $response['error'] ) . PHP_EOL;
                        throw new \Exception($response['error']['message']);
                    } elseif( $this->lean->getResponseModule() === 'domain' ) {
                        $response = $response['data'];
                    }
                } else if( $this->lean->getPrintResponse() || $this->success === false ) {
                    var_dump($response);
                }
            }

            return $response;
            
        //}
    }
}

class ResponseError {
    private $code;
    private $type;
    private $message;
    private $file;
    private $line;

    public function __construct( $pType, $pMessage, $pFile, $pLine ) {
        $this->message = $pMessage;
        $this->file = $pFile;
        $this->line = $pLine;
        $this->code = isset( $pType['code'] ) ? $pType['code'] : 0;
        $this->type = isset( $pType['type'] ) ? $pType['type'] : \Errors::getErrorType(0);
    }

    public function getErrorForResponse() {
        return [
            'code' => $this->code,
            'type' => $this->type,
            'message' => $this->message,
            'file' => $this->file,
            'line' => $this->line
        ];
    }
}