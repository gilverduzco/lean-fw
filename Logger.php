<?php
/* 
	Logs type:
		1. Error
		2. Request/Database
		3. Activity
*/

class Logger {
	private static $instance;
	private $lean;
	private $logErrorsDir;
	private $logRequestsDir;
	private $logActivityDir;
	private $loggerConfig;
	private $logOn;
	private $mongoConfig;
	private $inDateRequest;
	private $inController;
	private $dbTracking;
	private $params;

	//Construct.
	private function __construct() {
		$this->lean = Lean::getInstance();
		$this->logErrorsDir = 'errors';
		$this->logRequestsDir = 'requests';
		$this->logActivityDir = 'activity';
		$this->loggerConfig = $this->lean->getConfig('logger');
		$this->logOn = $this->loggerConfig ? true : false;
		$this->mongoConfig = $this->loggerConfig ? ( isset( $this->loggerConfig['mongo'] ) ? $this->loggerConfig['mongo'] : null ) : null;

		$this->initProperties();
	}

	public static function getInstance() {
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	//Inicializar las propiedades. (Por si el código es ejecutado en memoria)
	public function initProperties() {
		$this->inDateRequest = null;
		$this->dbTracking = array();
		$this->params = null;
	}

	private function validateIfCanLog( $pLogType ) {
		$canLog = false;
		
		if( $this->logOn === true && $this->mongoConfig && class_exists('\MongoDB\Driver\Manager')
			&& $pLogType >= 1 && $pLogType <= 3
		) {
			$canLog = true;
		}
		
		return $canLog;
	}
	
	//Método que sirve como interfaz para loguear.
	public function log( $pLogType, $pEvent = null, $pErrorType = null, $pFile = null, $pLine = null ) {
		try {
			if( $this->validateIfCanLog( $pLogType ) ) {
				$this->logToMongo( $pLogType, $pEvent, $pErrorType, $pFile, $pLine );
			}
		} 
		catch( MongoConnectionException $e ) {} 
		catch( MongoException $e ) {} 
		catch( Exception $e ) {}
	}

	public static function error( $pEvent = null, $pErrorType = null, $pFile = null, $pLine = null ) {
		Logger::getInstance()->log( 1, $pEvent, $pErrorType, $pFile, $pLine );
	}

	public static function activity( $pEvent ) {
		Logger::getInstance()->log( 3, $pEvent );
	}

	//Método para registrar la fecha de entrada del request.
	public function setInDateRequest() {
		$this->inDateRequest = new \DateTime('now', new \DateTimeZone('UTC'));
	}
	
	//Método para registrar el controller para cuando no exista uri.
	public function setController( $pController ) {
		$this->inController = $pController;
	}

	//Método para registrar cada petición que se hace a la bd.
	public function registerDBTracking( $pDBTracking ) {
		//Sólo se permitirá guardar 1000 posiciones para evitar que truene por "Allowed memory".
		if( count($this->dbTracking) < 1000 ) {
			$inDate = $pDBTracking['inDate'];
			$currentDate = new \DateTime('now', new \DateTimeZone('UTC'));
			$pDBTracking['inDate'] = new \MongoDB\BSON\UTCDateTime( $inDate->getTimestamp() * 1000 );
			$pDBTracking['outDate'] = new \MongoDB\BSON\UTCDateTime( $currentDate->getTimestamp() * 1000 );
			$pDBTracking['duration'] = $currentDate->getTimestamp() - $inDate->getTimestamp();
			
			array_push( $this->dbTracking, $pDBTracking );
		}
	}

	public function getDBTracking() {
		return $this->dbTracking;
	}

	//Método para registrar los parámetros para cuando se use el runController.
	public function setParams( $pParams ) {
		$this->params = $pParams;
	}

	//Método para ignorar los archivos que vienen en base64.
	private function ignoreBase64File( &$pBody ) {
		if( is_array( $pBody ) ) {
			foreach( $pBody as $key => $value ) {
				if( is_array( $value ) ) {
					$this->ignoreBase64File( $value );
				} else {
					//if( preg_match( '/^data:image\/(\w+);base64,/', $value, $type ) ) {
					if( preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $value) ) {
						unset( $pBody[$key] );
					}
				}
			}
		}
	}
	
	//Método para escribir el log en MongoDB.
	private function logToMongo( $pLogType, $pEvent = null, $pErrorType = null, $pFile = null, $pLine = null ) {
		$db = isset( $this->mongoConfig['db'] ) && $this->mongoConfig['db'] ? $this->mongoConfig['db'] : 'LeanLogs';
		$db .=  ( new \DateTime( 'now', new \DateTimeZone('UTC') ) )->format('Ymd');

		if( isset( $this->mongoConfig['host'] ) ) {
			$isCli = $this->lean->getIsCli();

			if( is_null($this->inDateRequest) ) $this->setInDateRequest();

			//Los options no se van a loguear
			if( $isCli === true || ( $isCli === false && $this->lean->getRouter()->getRequest()->getVerb() != 'OPTIONS' ) ) {
				$log = true;
				$currentDate = new \DateTime( 'now', new \DateTimeZone('UTC') );
				$collection = null;
				$document = $pLogType === 3 ? ( is_array( $pEvent ) ? $pEvent : array('event' => $pEvent) ) : array();
				$document['module'] = $this->lean->getResponseModule();
				$document['verb'] = $isCli === false ? $this->lean->getRouter()->getRequest()->getVerb() : 'no-verb';
				$document['baseUri'] = $isCli === false ? $this->lean->getRouter()->getRequest()->getBaseURI() : null;
				$document['uri'] = $isCli === false ? $this->lean->getRouter()->getRequest()->getUri() : $this->inController;
				$document['siteName'] = $this->lean->getConfig('siteName');
				
				//Parámetros de entrada.
				if( $isCli === false ) {
					$document['inParams'] = array(
						'headers' => $this->lean->getRouter()->getRequest()->getHeaders(),
						'params' => $this->lean->getRouter()->getRequest()->getParams(),
						'body' => null,
						//'bodyString' => $this->lean->getRouter()->getRequest()->getBodyString(),
						'query' => $this->lean->getRouter()->getRequest()->getQuery()
					);
					$bodyData = $this->lean->getRouter()->getRequest()->getBody();

					//No se deben loguear archivos que vienen en base64.
					if( $bodyData ) {
						$this->ignoreBase64File( $bodyData );
						$document['inParams']['body'] = $bodyData ? $bodyData : null;
					}

					//Validamos si se debe loggear.
					if( isset( $document['inParams']['body']['_log'] ) ) {
						$log = $document['inParams']['body']['_log'] ? true : false;
					} elseif( isset( $document['inParams']['query']['_log'] ) ) {
						$log = $document['inParams']['query']['_log'] ? true : false;
					}
				} else {
					$document['inParams'] = array('params' => $this->params);

					//Validamos si se debe loggear.
					if( isset( $document['inParams']['params']['_log'] ) ) {
						$log = $document['inParams']['params']['_log'] ? true : false;
					}
				}
				
				if( $pLogType === 1 || $log === true ) {
					$stringCnn = $this->mongoConfig['host'] . ':' . ( isset( $this->mongoConfig['port'] ) ? $this->mongoConfig['port'] : 27017 );
					$mongo = new \MongoDB\Driver\Manager( $stringCnn );
					$bulk = new \MongoDB\Driver\BulkWrite;

					if( $pLogType === 1 || $pLogType === 3 ) {
						if( $pLogType === 1 ) {
							$document['event'] = $pEvent;
						}

						$document['createdAt'] = new \MongoDB\BSON\UTCDateTime( $currentDate->getTimestamp() * 1000 );
					}
					
					if( $pLogType === 1 ) {
						$collection = $this->logErrorsDir;
						$document['errorCode'] = $pErrorType + 1;
						$document['file'] = $pFile;
						$document['line'] = $pLine;
						$document['dbTracking'] = $this->dbTracking;
					} else if( $pLogType === 2 ) {
						$collection = $this->logRequestsDir;
						$document['inDate'] = new \MongoDB\BSON\UTCDateTime( $this->inDateRequest->getTimestamp() * 1000 );
						$document['outDate'] = new \MongoDB\BSON\UTCDateTime( $currentDate->getTimestamp() * 1000 );
						$document['duration'] = $currentDate->getTimestamp() - $this->inDateRequest->getTimestamp();
						$document['dbTracking'] = $this->dbTracking;
					} else if( $pLogType === 3 ) {
						$collection = $this->logActivityDir;
					}
					
					//$collection .= ( new \DateTime( 'now', new \DateTimeZone('UTC') ) )->format('Ymd');
					$bulk->insert( $document );
					$mongo->executeBulkWrite( $db . '.' . $collection, $bulk );
				}
			}
		} else {
			throw new \Exception( 'Be sure to set the following properties for the logger mongo configuration: "host" and "port" (port is optional).' );
		}
		
		if( $pLogType === 2 ) $this->initProperties();
	}
}