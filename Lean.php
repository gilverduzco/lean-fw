<?php
/*
    Status code:
        200. OK
        404. Not Found
        405. Method not Allowed
        500. Internal Server Error
*/

class Lean {
    private static $instance;
    private $config;
    private $environment;
    private $debug;
    private $router;
    private $response;
    private $controllers;
    private $domains;
    private $models;
    private $data;
    private $customMessages;
    private $responseModule;
    private $language;
    private $isCli;
    private $unitTestsMode;
    private $isRESTful;
    private $xss;
    private $onFinish;
    private $onFinishLastPosition;
    private $printError;
    private $printResponse;

    private function __construct() {
        $this->initProperties();
    }

    public static function getInstance() {
        if( !self::$instance instanceof self ) {
            self::$instance = new self();
            self::$instance->loadConfig();
        }

        return self::$instance;
    }

    public static function destroy() {
        self::$instance = null;
    }

    public function getConfig( $pKey = null ) {
        return $pKey ? ( isset( $this->config[$pKey] ) ? $this->config[$pKey] : null ) : $this->config;
    }

    //Inicializar las propiedades. (Por si el código es ejecutado en memoria)
    public function initProperties() {
        $this->config = null;
        $this->customMessages = array(
            'pk' => array(),
            'fk' => array(),
            'uq' => array()
        );
        $this->responseModule = null;
        $this->language = null;
        $this->printError = true;
        $this->isCli = ( php_sapi_name() == 'cli' ? true : false );
        $this->controllers = array();
        $this->domains = array();
        $this->models = array();
        $this->data = array();
        $this->onFinish = array( array() );
        $this->onFinishLastPosition = 0;
    }

    public function loadConfig() {
        $config = array();
        $configFiles = _CONFIG . '/*.config.php';
        $configFiles = glob($configFiles);
        $configFilesCount = count( $configFiles );

        for( $x=0; $x<$configFilesCount; $x++ ) {
            $fileName = str_replace( _CONFIG . '/', '', str_replace( '.config.php', '', $configFiles[$x] ) );
            $names = explode('-', $fileName);
            $namesCount = count( $names );
            $fileName = '';
            
            for( $y=0; $y<$namesCount; $y++ ) { 
                if( $y === 0 ) {
                    $fileName .= $names[$y];
                } else {
                    $fileName .= ucfirst( $names[$y] );
                }
            }

            if( $fileName == 'app' ) {
                $config = array_merge( $config, require $configFiles[$x] );
            } else {
                $config = array_merge( $config, [ $fileName => require $configFiles[$x] ] );
            } 
        }

        $this->setConfig($config);
    }

    public function setConfig( $pConfig ) {
        $this->config = $pConfig;
        $this->debug = isset( $this->config['debug'] ) && $this->config['debug'];
        $this->isRESTful = isset( $this->config['RESTful'] ) ? $this->config['RESTful'] : true;
        $this->xss = isset( $this->config['xss'] ) ? $this->config['xss'] : true;
        $timeZone = isset( $this->config['timeZone'] ) ? $this->config['timeZone'] : null;
        $this->setEnv( $this->config['environment'] ? $this->config['environment'] : 'production' );
            
        if( $timeZone ) {
            date_default_timezone_set($timeZone);
        }
    }

    public function reloadConfig() {
        $this->loadConfig();
    }

    public function mergeConfig( $pConfig ) {
        $this->config = array_merge( $this->config, $pConfig );
    }

    private function getRouteMethodParamsFromCli() {
        $routeMethod = null;
        $params = null;
        $argvCount = count($_SERVER['argv']);
        
        for( $x=1; $x<$argvCount; $x++ ) {
            $param = explode( '--', $_SERVER['argv'][$x] );
            $guion = substr($_SERVER['argv'][$x], 0, 2);
    
            if( $guion === '--' && count( $param ) == 2 ) {
                $param = explode( '=', $param[1] );
                $params[ $param[0] ] = $param[1];
            } else if( $x===1 ) {
                $routeMethod = $_SERVER['argv'][$x];
            }
        }

        return array(
            'routeMethod' => $routeMethod,
            'params' => $params
        );
    }

    public static function init( $pConfigRequestType = null ) {
        try {
            $lean = self::getInstance();

            if( is_numeric($pConfigRequestType) ) {
                $lean->setIsCli( !( $pConfigRequestType === _HTTP ) );
                $lean->setUnitTestsMode( $pConfigRequestType === _UNIT_TESTS );
            }

            if( $lean->getIsCli() === false || $lean->getUnitTestsMode() === true ) {
                $lean->router = new \Router();
            }
            
            //Cacharemos los errores para manipularlos.
            register_shutdown_function(function() use($lean) {
                if( ( $error = error_get_last() ) ) {
                    $lean->response = new \Response();
                    //if( $lean->response ) {
                        \Errors::uncaught( $error['message'], null, $error['file'], $error['line'] );
                        $lean->response->send(null);
                    //}
                }
            });

            if( $lean->getIsCli() === false ) {
                set_exception_handler(function($e) use($lean) {
                    $lean->response = new \Response();
                    //if( $lean->response ) {
                        \Errors::uncaught( $e->getMessage(), null, $e->getFile(), $e->getLine() );
                        $lean->response->send(null);
                    //}
                });
            }
        } catch( \Errors $e ) {
            $lean->setPrintError(false);
        } catch( \Exception $e ) {
            $lean->setPrintError(false);
            \Errors::exception( $e->getMessage(), null, $e->getFile(), $e->getLine() );
        }  

        return $lean;
    }

    public function run( $pRouteMethod = null, $pParams = null ) {
        if( is_null($this->config) ) {
            $this->loadConfig();
        }
        
        $this->response = new \Response();
        
        if( $this->getIsCli() === true ) {
            $type = 2;
            
            if( $this->getUnitTestsMode() === true ) {
                $type = strpos( $pRouteMethod, 'domain:' ) === false ? 4 : 3;
            }
            
            if( is_null($pRouteMethod) ) {
                $data = $this->getRouteMethodParamsFromCli();
                
                $pRouteMethod = $data['routeMethod'];
                $pParams = $data['params'];

                $this->setPrintResponse(true);
            }
        } else {
            ob_start();
            $type = 1;
        }

        return $this->runCT( $pRouteMethod, $pParams, $type );
    }

    private function runCT( $pRouteMethod, $pParams, $pType ) {
        try {
            $success = true;
            $data = null;
            $logger = \Logger::getInstance();
            $logger->setInDateRequest();
            $logger->setParams($pParams);
            
            if( $pType === 1 ) { //API
                $this->router->init();
                $this->setResponseModule( $this->router->getModule() );
                $this->language = $this->loadLanguage(); //Definimos el lenguaje.
                $data = $this->router->run();
            } elseif( $pType === 2 ) { //Console
                $logger->setController( $pRouteMethod );
                $this->setResponseModule('cli');
                $routeData = $this->getRouteMethod( 'cli:' . $pRouteMethod, true );
                $class = $routeData['class'];
                $method = $routeData['method'];
                $objClass = \Helper::getController( 'cli:' . $pRouteMethod );

                if( !method_exists( $objClass, $method ) ) {
                    throw new \ErrorsDeveloper( 'The method "' . $method . '" does not exist in class "' . $class . '"' );
                }

                $data = $objClass->$method( $pParams );
            } elseif( $pType === 3 ) { //Unit Tests Domains
                $this->getRouter()->getRequest()->setAllParams($pParams);
                $this->setResponseModule('domain');
                $routeData = $this->getRouteMethod( $pRouteMethod, false );
                $class = $routeData['class'];
                $method = $routeData['method'];
                $objClass = \Helper::getDomain( $routeData['folder'] . '/' . $routeData['file'] );
                
                if( !method_exists( $objClass, $method ) ) {
                    throw new \ErrorsDeveloper( 'The method "' . $method . '" does not exist in class "' . $class . '"' );
                }

                $reflection = new \ReflectionClass( get_class($objClass) );
                $method = $reflection->getMethod($method);
                $method->setAccessible(true);
                $data = $method->invokeArgs($objClass, $pParams);
            } elseif( $pType === 4 ) { //Unit Tests Controllers
                $this->getRouter()->getRequest()->setAllParams($pParams);
                $routeData = $this->getRouteMethod( $pRouteMethod, true );
                $this->setResponseModule( $routeData['module'] );
                $class = $routeData['class'];
                $method = $routeData['method'];
                $objClass = \Helper::getController( $pRouteMethod );

                if( !method_exists( $objClass, $method ) ) {
                    throw new \ErrorsDeveloper( 'The method "' . $method . '" does not exist in class "' . $class . '"' );
                }

                $data = $objClass->$method($pParams);
            }
        } catch( \Errors $e ) {
            $success = false;
            $this->setPrintError(false);
        } catch( \Exception $e ) {
            $success = false;
            $this->setPrintError(false);
            \Errors::exception( $e->getMessage(), null, $e->getFile(), $e->getLine() );
        } 
        
        //Nos aseguramos de cerrar las conexiones.
        try {
            $errorGetLast = error_get_last();

            /*if( $errorGetLast && preg_match( '/fsockopen()/i', $errorGetLast['message'] ) ) {
                $errorGetLast = null;
            }*/

            $commit = !$errorGetLast && $success;

            DBAccess::closeConnections( $commit );

            if( $commit && $this->onFinish ) {
                $onFinishCount = count($this->onFinish);

                for( $xOnFin=0; $xOnFin<$onFinishCount; $xOnFin++ ) {
                    $onFinishCountX = count( $this->onFinish[$xOnFin] );

                    for( $yFnc=0; $yFnc<$onFinishCountX; $yFnc++ ) {
                        if( $this->onFinish[$xOnFin][$yFnc] ) $this->onFinish[$xOnFin][$yFnc]();
                    }
                }
            }

            if( $errorGetLast ) {
                $success = false;
                $this->setPrintError(false);
                \Errors::uncaught( $errorGetLast['message'], null, $errorGetLast['file'], $errorGetLast['line'] );
            }

            return $this->response->send($data);
        } catch( \Errors $e ) {
            $success = false;
            $this->setPrintError(false);
        } catch( \Exception $e ) {
            $success = false;
            $this->setPrintError(false);
            \Errors::exception( $e->getMessage() . ' => ' . $e->getTraceAsString(), null, $e->getFile(), $e->getLine() );

            if( $this->getUnitTestsMode() === true ) {
                throw new \Exception($e->getMessage());
            }
        } finally {
            $this->initProperties();
        }
    }

    public function getRouteMethod( $pRouteMethod, $pIsController ) {
        $params = null;
        $routeArr = explode( '@', $pRouteMethod );
        $resource = 'controller';
        $resourceClass = 'Controller';
        $errorMsg = 'Naming error for route controller (' . $pRouteMethod . ').';
        
        if( count($routeArr) !== 2 ) {
            throw new \ErrorsDeveloper($errorMsg);
        }
        
        $classData = $this->getClass($routeArr[0], $pIsController);
        $classData['method'] = $routeArr[1];

        return $classData;
    }

    private function getClass( $pRouteMethod, $pIsController ) {
        $errorMsg = 'Naming error for class (' . $pRouteMethod . ').';
        $folder = '';
        $class = '';
        $module = null;
        $routeMethod = $pRouteMethod;
        $moduleArr = explode( ':', $pRouteMethod );

        if( $pIsController === true && count($moduleArr) !== 2 ) {
            throw new \ErrorsDeveloper($errorMsg);
        }

        $routeMethod = isset( $moduleArr[1] ) ? $moduleArr[1] : $moduleArr[0];

        if( $pIsController ) {
            $module = $moduleArr[0];
            $folder = 'controllers';
        }

        $routeArr = explode( '/', $routeMethod );
        $routeArrCount = count($routeArr);
        $file = null;
        
        if( $routeArrCount === 1 ) {
            $file = $routeArr[0];
        } else {
            for( $x=0; $x<$routeArrCount; $x++) {
                if( $x === ($routeArrCount - 1) ) {
                    $file = $routeArr[$x];
                } else {
                    $folder .= ( $folder === '' ? '' : '/' ) . $routeArr[$x];
                }
            }
        }
        
        $folder = ( $module ? $module . '/' : '' ) . $folder;
        $names = explode('-', $file);

        foreach( $names as $name ) {
            $class .= ucfirst($name);
        }

        return array(
            'module' => $module,
            'folder' => $folder,
            'file' => $file,
            'class' => $class
        );
    }

    public function getIsCli() {
        return $this->isCli;
    }

    public function setIsCli( $pIsCli ) {
        return $this->isCli = $pIsCli;
    }

    public function setUnitTestsMode( $pUnitTestsMode ) {
        return $this->unitTestsMode = $pUnitTestsMode;
    }

    public function getUnitTestsMode() {
        return $this->unitTestsMode;
    }

    public function getIsRESTful() {
        return $this->isRESTful;
    }

    public function setIsRESTful( $pIsRESTful ) {
        $this->isRESTful = $pIsRESTful;
    }

    public function getRouter() {
        return $this->router;
    }

    public function getDebug() {
        return $this->debug;
    }

    public function setXSS( $pXSS ) {
        $this->xss = $pXSS;
    }

    public function getXSS() {
        return $this->xss;
    }

    public function getPrintError() {
        return $this->printError;
    }

    public function setPrintError( $pPrintError ) {
        $this->printError = $pPrintError;
    }

    public function getPrintResponse() {
        return $this->printResponse;
    }

    public function setPrintResponse( $pPrintResponse ) {
        $this->printResponse = $pPrintResponse;
    }

    //Método para leer los archivos .json.
    public function getJSONFile( $pResource, $pFileName ) {
        $contentFile = null;
        $stringsFile = glob( _APP . '/' . $pResource . '/' . $pFileName . '.json' );

        if( count( $stringsFile ) === 1 ) {
            $contentFile = json_decode( file_get_contents( $stringsFile[0] ), true ); 
        }
        
        return $contentFile;

    }

    //Método para detectar el idioma del navegador.
    public function loadLanguage() {
        $language = $this->getConfig('defaultLanguage');
        $language = $language ? $language : 'es';

        if( $this->getIsCli() === false ) {
            $forcedLanguage = $this->getRouter()->getRequest()->getHeaders('_language');
            
            if( $forcedLanguage ) {
                $language = $forcedLanguage;
            } else {
                if( isset( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) ) {
                    $langs = array_reduce(
                        explode( ',', $_SERVER['HTTP_ACCEPT_LANGUAGE'] ), function( $res, $el ){
                            list( $l, $q ) = array_merge( explode( ';q=', $el ), [1] );
                            $res[$l] = (float) $q;
                            return $res;
                        }, []);

                    arsort( $langs, SORT_NUMERIC );                        

                    //Extract most important (first)
                    foreach( $langs as $lang => $val ){ break; }

                    //If complex language simplify it
                    if( stristr( $lang, '-' ) ) {
                        $language = explode( '-', $lang )[0];
                    }
                }
            }
        }
        
        return $this->availableLanguage($language);
    }

    //Pendiente agregar a cache.
    private function availableLanguage( $pLanguage ) {
        $availableLanguage = $pLanguage;
        $languages = glob( _APP . '/strings/*' );
        $count = count($languages);
        $strings = $this->getJSONFile( 'strings', $pLanguage . '.string' );

        if( is_null( $strings ) && $count > 0 ) {
            $availableLanguage = explode( '/', $languages[0] );
            $availableLanguage = explode( '.', $availableLanguage[ count($availableLanguage) - 1 ] )[0];
        }
        
        return $availableLanguage;
    }

    //Método para definir el módulo que debe dar respuesta.
    public function setResponseModule( $pResponseModule ) {
        $this->responseModule = $pResponseModule;
    }

    public function getResponseInstance() {
        return $this->response;
    }
    
    public function getResponseModule() {
        return $this->responseModule;
    }

    //Método para establecer el ambiente.
    public function setEnv( $pEnvironment ) {
        $this->environment = strtolower( trim( $pEnvironment ) );

        if( $this->getDebug() ) {
            error_reporting(E_ALL);
        } else {
            error_reporting(0);
        }
    }

    //Método para obtener el ambiente.
    public function getEnv() {
        return $this->environment;
    }

    //Método para obtener el request verb.
    public function getRequestVerb() {
        return $this->router->getRequest()->getVerb();
    }

    //Método para obtener el lenguaje.
    public function getLanguage() {
        return $this->language;
    }

    //Método para instanciar los Controllers.
    public function getController( $pRoute, $pIdInstance = null, $pParams = null ) {
        $id = $pRoute;
        $params = $pParams;
        $routeData = $this->getRouteMethod( $pRoute, true );
        $module = $routeData['module'];
        $folder = $routeData['folder'];
        $file = $routeData['file'];
        $class = $routeData['class'] . 'Controller';
        
        //Validate id for class.
        if( !is_null( $pIdInstance ) ) {
            //Si el "$pId" viene como array, significa que se usará como los parámetros.
            if( is_array( $pIdInstance ) ) {
                $params = $pIdInstance;
            } else {
                $id .= $pIdInstance;
            }
        }

        $id = md5( $id );
        
        //Si no existe, la instanciamos.
        if( !isset( $this->controllers[ $id ] ) ) {
            $path = _MODULES . '/' . $folder . '/' . $file . '.controller.php';

            if( glob( $path ) ) {
                require_once $path;

                //Validate class.
                $this->validateClass( $class );

                if( !is_null( $params ) ) {
                    $this->controllers[ $id ] = new $class( $params );
                } else {
                    $this->controllers[ $id ] = new $class();
                }
            } else {
                throw new \ErrorsDeveloper( 'Controller file not found (' . $pRoute . ').' );
            }
        }

        return $this->controllers[ $id ];
    }

    //Método para instanciar los Dominios.
    public function getDomain( $pRoute, $pIdInstance = null, $pParams = null ) {
        $id = $pRoute;
        $params = $pParams;
        $classData = $this->getClass($pRoute, false);
        $class = $classData['class'] . 'Domain';

        //Validate id for class.
        if( !is_null( $pIdInstance ) ) {
            //Si el "$pId" viene como array, significa que se usará como los parámetros.
            if( is_array( $pIdInstance ) ) {
                $params = $pIdInstance;
            } else {
                $id .= $pIdInstance;
            }
        }

        $id = md5( $id );
        
        //Si no existe, la instanciamos.
        if( !isset( $this->domains[ $id ] ) ) {
            $path = _APP . '/domains/' . $pRoute . '.domain.php';

            if( glob( $path ) ) {
                require_once $path;

                //Validate class.
                $this->validateClass( $class );

                if( !is_null( $params ) ) {
                    $this->domains[ $id ] = new $class( $params );
                } else {
                    $this->domains[ $id ] = new $class();
                }
            } else {
                throw new \ErrorsDeveloper( 'Domain file not found (' . $pRoute . ').' );
            }
        }

        return $this->domains[ $id ];
    }

    private function validateModelTransaction( $pExtraConfig ) {
        $isDefined = true;

        if( isset($pExtraConfig['withTransaction']) ) {
            $withTransaction = $pExtraConfig['withTransaction'];
        }

        return $withTransaction;
    }

    //Método para obtener acceso a la capa de datos.
    public function getModel( $pRoute, $pCnn = null, $pExtraConfig = null ) {
        $id = $pRoute;
        $classData = $this->getClass($pRoute, false);
        // $file = $classData['file'];
        $class = $classData['class'] . 'Model';
        $transaction = isset($pExtraConfig['transaction']) ? $pExtraConfig['transaction'] : true;

        //Validate id for class.
        if( !is_null( $pCnn ) ) {
            $idCnn = $pCnn;

            if( is_array( $pCnn ) ) {
                //Validamos que exstan los datos de conexión necesarios.
                if( isset( $pCnn['dbType'] )
                    && isset( $pCnn['host'] )
                    && isset( $pCnn['user'] )
                    && isset( $pCnn['password'] )
                    && isset( $pCnn['db'] ) 
                ) {
                    //Se crea un id de conexión dinámico. Debido a que el programador definió los datos de conexión en tiempo de ejecución.
                    $idCnn = 'leanDataSource_' . $pCnn['dbType']
                            . '_' . $pCnn['host']
                            . '_' . $pCnn['db']
                            . '_' . $pCnn['user']
                            . '_' . ( isset( $pCnn['port'] ) ? $pCnn['port'] : '' );
                } else {
                    throw new \ErrorsDeveloper( 'Be sure to set the following properties for the "getModel" method: "dbType", "host", "user", "password" and "db".' );
                }
            } else if( !is_string( $pCnn ) ) {
                throw new \ErrorsDeveloper( 'Incorrect parameter for "getModel". Must receive a "string" or an "array".' );
            }

            $id .= $idCnn;
        }

        $id = md5( $id . ( $transaction === false ? '_noTransaction' : '' ) );

        //Si no existe, la instanciamos.
        if( !isset( $this->models[ $id ] ) ) {
            $path = _APP . '/models/' . $pRoute . '.model.php';

            if( glob( $path ) ) {
                require_once $path;

                //Validate class.
                $this->validateClass( $class );

                $this->models[ $id ] = new $class( $pCnn, $pExtraConfig );
            } else {
                throw new \ErrorsDeveloper( 'Model file not found (' . $pRoute . ').' );
            }
        }

        return $this->models[ $id ];
    }

    //Método para establecer información.
    public function setData( $pIndex, $pData ) {
        $this->data[ $pIndex ] = $pData;
    }

    //Método para obtener la información.
    public function getData( $pIndex = null, $pDelete = true ) {
        if( is_null( $pIndex ) ) {
            $data = $this->data;

            if( $pDelete ) {
                $this->data = array();
            }
        } else {
            $data = $this->data[ $pIndex ];

            if( $pDelete ) {
                unset( $this->data[ $pIndex ] );
            }
        }

        return $data ;
    }

    public function getCustomMessages( $pType = null ) {
        if( is_null($pType) ) {
            return $this->customMessages;
        } else {
            if( isset( $this->customMessages[$pType] ) ) {
                return $this->customMessages[$pType];
            }

            throw new \ErrorsDeveloper('Custom constraint key does not exist (' . $pType . ')');
        }
    }

    public function setCustomMessage( $pType, $pConstraint, $pMessage ) {
        $this->customMessages[$pType][$pConstraint] = array(
            'message' => $pMessage
        );
    }

    //Método para validar las reglas.
    public function checkPolicies( $pFileName, $pData, $pPolicyKey = null, $pExtraValues = null ) {
        $module = $this->getResponseModule();
        $rules = '';
        $policies = null;
        $policyRoutefile = _MODULES . '/' . $module . '/policies/' . $pFileName . '.policy.php';

        if( count( glob( $policyRoutefile ) ) ) {
            $policies = require $policyRoutefile;

            if( $pPolicyKey ) {
                if( isset( $policies[$pPolicyKey] ) && is_array( $policies[$pPolicyKey] ) ) {
                    $policies = $policies[$pPolicyKey];
                } else {
                    throw new \ErrorsDeveloper( 'Policies in "' . $pPolicyKey . '" are not found, file: "' . $policyRoutefile . '", module: "' . $module . '".' );
                }
            }
        }

        if( is_null( $policies ) ) {
            throw new \ErrorsDeveloper( 'Policy file (' . $policyRoutefile . ') not found in module "' . $module . '".' );
        }

        foreach( $policies as $keyRule => $valueRule ) {
            if( isset( $valueRule['constraint'] ) ) {
                $constraints = $valueRule['constraint'];
                $constraint = null;
                $value = isset( $pData[ $keyRule ] ) ? $pData[ $keyRule ] : null;
                $gtVal = null;

                //Apply trim.
                if( is_string( $value ) ) {
                    $value = trim( $value );
                }

                //Loop through constraints.
                foreach( $valueRule['constraint'] as $keyConstraint => $valConstraint ) {
                    if( $rules ) {
                        break;
                    }
                    
                    //Validate message to show.
                    $message =
                        isset( $valueRule['messages'] ) && isset( $valueRule['messages'][ $keyConstraint ] )
                            ? $valueRule['messages'][ $keyConstraint ]
                            : ( isset( $valueRule['message'] )
                                ? $valueRule['message']
                                : null );

                    switch( $keyConstraint ) {
                        case 'required':
                            if( $valConstraint && ( is_null( $value ) || $value === '' ) ) {
                                $rules = $message;
                            }

                            break;

                        case 'gt':
                            if( is_null( $value ) || !is_numeric( $value ) || !( $value * 1 > $valConstraint ) ) {
                                $rules = str_replace( '{gt}', $valConstraint, $message );
                            }

                            break;

                        case 'gte':
                            if( is_null( $value ) || !is_numeric( $value ) || !( $value * 1 >= $valConstraint ) ) {
                                $rules = str_replace( '{gte}', $valConstraint, $message );
                            }

                            break;

                        case 'lt':
                            if( is_null( $value ) || !is_numeric( $value ) || !( $value * 1 < $valConstraint ) ) {
                                $rules = str_replace( '{lt}', $valConstraint, $message );
                            }

                            break;

                        case 'lte':
                            if( is_null( $value ) || !is_numeric( $value ) || !( $value * 1 <= $valConstraint ) ) {
                                $rules = str_replace( '{lte}', $valConstraint, $message );
                            }

                            break;

                        case 'email':
                            if( $valConstraint && ( $value && !filter_var( $value, FILTER_VALIDATE_EMAIL ) ) ) {
                                $rules = $message;
                            }

                            break;

                        case 'minLength':
                            if( $value && strlen( $value ) < $valConstraint ) {
                                $rules = $message;
                            }

                            break;

                        case 'maxLength':
                            if( $value && strlen( $value ) > $valConstraint ) {
                                $rules = $message;
                            }

                            break;
                    }
                }
            } else {
                throw new \ErrorsDeveloper('It is neccesary to define the attribute "constraint" for the element: "' . $keyRule . '".');
            }
        }

        if( $rules !== '' ) {
            throw new \ErrorsController( $rules, $pExtraValues );
        }
    }

    //Método para validar si la clase existe.
    private function validateClass( $pClassName ) {
        if( !class_exists( $pClassName ) ) {
            throw new \ErrorsDeveloper( 'Class "' . $pClassName . '" not found.' );
        }
    }

    public function onFinish( $pFnc ) {
        if( $pFnc ) {
            $onFinCount = count($this->onFinish[$this->onFinishLastPosition]);

            if( $onFinCount > 0 && $onFinCount % 800 == 0 ) {
                $this->onFinish[ ++$this->onFinishLastPosition ] = array();
            }
            
            array_push( $this->onFinish[ $this->onFinishLastPosition ], $pFnc );  
        }
    }
}