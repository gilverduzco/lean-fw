<?php
class Auth {
    private static $secret = 'j¡w@t&l$e#a=n';

    //Método para crear el token basado en JWT.
    public static function signIn( $pData, $pExp = 3600, $pSecret = null ) {
        return \Firebase\JWT\JWT::encode(array(
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + $pExp,
            'aud' => self::aud(),
            'data' => $pData
        ), is_null( $pSecret ) ? self::$secret : $pSecret );
    }

    //Método para verificar el token.
    public static function verify( $pToken, $pSecret ) {
        $lean = \Lean::getInstance();
        $data = null;

        try {
            $data = \Firebase\JWT\JWT::decode( $pToken, $pSecret, array('HS256') );

            /*if( property_exists( $data, 'aud' ) && $data->aud !== self::aud() ) {
                throw new Exception( 'Invalid user logged in.' );
            }*/
        } catch( \Exception $e ) {
            throw new \ErrorsAuth( 'Session has expired.' );
        }

        $data = (array) $data->data;
        $lean->getRouter()->getRequest()->setTokenData($data);

        return $data;
    }

    //Método para verificar el token.
    /*public static function verify( $pToken, $pSecret ) {
        $lean = \Lean::getInstance();
        $data = null;
        $headers = new stdClass();

        try {
            $data = \Firebase\JWT\JWT::decode($pToken, new \Firebase\JWT\Key($pSecret, 'HS256'), $headers);
        } catch( \Exception $e ) {
            throw new \ErrorsAuth( 'Session has expired.' );
        }

        $data = (array) $data->data;
        $lean->getRouter()->getRequest()->setTokenData($data);

        return $data;
    }*/

    private static function aud() {
        $aud = isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
        /*$ip = '';

        //Cloudflare
        if( isset( $_SERVER['HTTP_CF_CONNECTING_IP'] ) ) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
        }
        
        $httpClientIP = isset( $_SERVER['HTTP_CLIENT_IP'] ) ? $_SERVER['HTTP_CLIENT_IP'] : null;
        $httpXForwardedFor = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : null;
        $remoteAddr = isset( $_SERVER['REMOTE_ADDR'] ) ? $_SERVER['REMOTE_ADDR'] : null;

        if( filter_var( $httpClientIP, FILTER_VALIDATE_IP ) ) { $ip = $httpClientIP; }
        elseif( filter_var( $httpXForwardedFor, FILTER_VALIDATE_IP ) ) { $ip = $httpXForwardedFor; }
        else { $ip = $remoteAddr; }

        $aud .= $ip;*/

        return sha1($aud);
    }
}