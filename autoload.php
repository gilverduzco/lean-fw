<?php
//CONSTANTS.
define( '_ROOT', str_replace( '/lean/..', '', __DIR__ . '/..' ) );
define( '_LEAN', _ROOT . '/lean' );
define( '_CONFIG', _ROOT . '/config' );
define( '_APP', _ROOT . '/app' );
define( '_MODULES', _APP . '/modules' );
define( '_HTTP', 1 );
define( '_CLI', 2 );
define( '_UNIT_TESTS', 3 );

//REQUIRES.
require _LEAN . '/Lean.php';
require _LEAN . '/Logger.php';
require _LEAN . '/Helper.php';
require _LEAN . '/Errors.php';
require _LEAN . '/Cache.php';
require _LEAN . '/BaseController.php';
require _LEAN . '/BaseMiddleware.php';
require _LEAN . '/BaseDomain.php';
require _LEAN . '/BaseModel.php';
require _LEAN . '/Router.php';
require _LEAN . '/Request.php';
require _LEAN . '/Auth.php';
require _LEAN . '/Response.php';
require _LEAN . '/DBAccess.php';

if( class_exists( 'PHPUnit\Framework\TestCase' ) ) {
    require _LEAN . '/BaseTest.php';
}