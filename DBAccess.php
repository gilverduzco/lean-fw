<?php
class DBAccess {
	//Propiedades de la clase.
	private static $lean;
	private static $instances = array();
	private static $cnns = array();
	private static $cache = array();
	private static $rememberTime;
	private $idCnn;
	private $cnnData;
	private $queryArray;
	private $totalRowsCache;
	private $locks;
	private $extraConfig;


	//Constructor.
	private function __construct( $pData, $pExtraConfig ) {
		$this->initProperties();
		$this->idCnn = $pData['idCnn'];
		$this->cnnData = $pData['cnnData'];
		$this->extraConfig = is_null($pExtraConfig) ? [] : $pExtraConfig;
		$this->extraConfig['transaction'] = isset($pExtraConfig['transaction']) ? $pExtraConfig['transaction'] : true;
	}

	//Método para obtener la instancia de conexión.
	public static function getInstance( $pCnn = null, $pExtraConfig = null ) {
		self::$lean = \Lean::getInstance();
		$cnnData = self::setConnection($pCnn, $pExtraConfig);

		if( !isset( self::$instances[ $cnnData['idCnn'] ] ) ) {
			self::$instances[ $cnnData['idCnn'] ] = new self($cnnData, $pExtraConfig);
		}

		return self::$instances[ $cnnData['idCnn'] ];
	}

	//Inicializar las propiedades. (Por si el código es ejecutado en memoria)
	private function initProperties() {
		$this->idCnn = null;
		$this->cnnData = null;
		$this->queryArray = array();
		$this->totalRowsCache = array();
		$this->locks = array(
			'PostgreSQL' => array(),
			'SQLServer' => array(),
			'MySQL' => array()
		);
		$this->extraConfig = null;
	}

	//Método para establecer los datos de conexión.
	private static function setConnection( $pCnn, $pExtraConfig = null ) {
		$dbConnections = \Helper::getConfig('dbConnections');
		$idCnn = $pCnn;
		$transaction = isset($pExtraConfig['transaction']) ? $pExtraConfig['transaction'] : true;
		$cnnData = null;
		
		//Validate dbConnections.
		if( !is_array( $pCnn ) && !$dbConnections ) {
			throw new \ErrorsDeveloper( 'There is not data in your "db-connections" file' );
		}

		//Set id connection.
		if( is_null( $pCnn ) ) {
			if( !( isset( $dbConnections['default'] ) && $dbConnections['default'] ) ) {
				throw new \ErrorsDeveloper( 'There is not a default connection in your "dataSource" property' );
			}

			$idCnn = $dbConnections['default'];
		}

		if( is_array( $idCnn ) ) {
			//Validatemos que vengan definidas las propiedad principales.
			if( isset( $idCnn['dbType'] )
				&& isset( $idCnn['host'] )
				&& isset( $idCnn['user'] )
				&& isset( $idCnn['password'] )
				&& isset( $idCnn['db'] ) ) {
				//Se crea un id de conexión dinámico. Debido a que el programador definió los datos de conexión en tiempo de ejecución.
				$cnnData = array(
					'host' => $idCnn['host'],
					'user' => $idCnn['user'],
					'password' => $idCnn['password'],
					'db' => $idCnn['db'],
					'dbType' => $idCnn['dbType'],
					'spPrefix' => isset( $idCnn['spPrefix'] ) ? $idCnn['spPrefix'] : '',
					'port' => isset( $idCnn['port'] ) ? $idCnn['port'] : '',
					'connStr' => isset( $idCnn['connStr'] ) ? $idCnn['connStr'] : '',
					'schema' => isset( $idCnn['schema'] ) ? $idCnn['schema'] : 'public'
				);
				$idCnn = 'leanDataSource_' . $idCnn['dbType']
					. '_' . $idCnn['host']
					. '_' . $idCnn['db']
					. '_' . $idCnn['user']
					. '_' . ( isset( $idCnn['port'] ) ? $idCnn['port'] : '' );
			} else {
				throw new \ErrorsDeveloper( 'Be sure to set the following properties for the "getInstance" method: "dbType", "host", "user", "password" and "db"' );
			}
		} else if( is_string( $idCnn ) ) {
			if( isset( $dbConnections['dataSource'] ) && $dbConnections['dataSource'] ) {
				//Validamos si la conexión está registrada.
				if( !isset( $dbConnections['dataSource'][ self::$lean->getEnv() ][ $idCnn ] ) ) {
					throw new \ErrorsDeveloper( 'The database identifier (' . $idCnn . ') is not registered for the environment "' . self::$lean->getEnv() . '"' );
				} else {
					$cnnData = $dbConnections['dataSource'][ self::$lean->getEnv() ][ $idCnn ];
					$cnnData['schema'] = isset( $cnnData['schema'] ) ? $cnnData['schema'] : 'public';
				}
			} else {
				throw new \ErrorsDeveloper( 'The "dataSource" property must be a valid array' );
			}
		} else {
			throw new \ErrorsDeveloper( 'Incorrect parameter for "getInstance". Must receive a "string" or an "array".' );
		}
		
		//Regresamos los datos de conexión
		return array(
			'idCnn' => $idCnn . ( $transaction === false ? '_noTransaction' : '' ),
			'cnnData' => $cnnData
		);
	}

	public static function getNewConnection( $pCnnData ) {
		$newConnection = null;

		if( 
			$pCnnData['dbType'] === 'MySQL' || 
			$pCnnData['dbType'] === 'PostgreSQL' || 
			$pCnnData['dbType'] === 'SQLServer' 
		) {
			//Validamos si el programador definió el connctionsString.
			if( isset( $pCnnData['connStr'] ) && trim( $pCnnData['connStr'] ) ) {
				$connStr = $pCnnData['connStr'];
				$connStr = str_replace( '{db}', $pCnnData['db'], $connStr );
				$connStr = str_replace( '{host}', $pCnnData['host'], $connStr );
				$connStr = str_replace( '{port}', $pCnnData['port'], $connStr );
			} else {
				$connStr = ( $pCnnData['dbType'] === 'MySQL'
						? 'mysql'
						: ( $pCnnData['dbType'] === 'PostgreSQL' ? 'pgsql' : 'sqlsrv' )
					)
					. ':' . ( $pCnnData['dbType'] === 'SQLServer' ? 'Database' : 'dbname' ) . '=' . $pCnnData['db']
					. ';' . ( $pCnnData['dbType'] === 'SQLServer' ? 'Server' : 'host' ) . '=' . $pCnnData['host'];

				//Validate port.
				if( isset( $pCnnData['port'] ) && trim( $pCnnData['port'] ) ) {
					if( $pCnnData['dbType'] === 'SQLServer' )
						$connStr .= ',' . $pCnnData['port'];
					else
						$connStr .= ';port=' . $pCnnData['port'];
				}
			}

			$newConnection = new \PDO( $connStr, $pCnnData['user'], $pCnnData['password'] );

			if( !$newConnection ) {
				throw new Exception( 'Failed to connect to database "' . $pCnnData['db'] . '".' );
			}

			$newConnection->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );

			//SQLServer no soporta esto.
			if( $pCnnData['dbType'] === 'MySQL' || $pCnnData['dbType'] === 'PostgreSQL' ) {
				$newConnection->setAttribute( \PDO::ATTR_EMULATE_PREPARES, true );
			}

			if( isset( $pCnnData['charset'] ) && version_compare( PHP_VERSION, '5.3.6', '<' ) ) {
				$newConnection->setAttribute( \PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES '" . $pCnnData['charset'] . "'" );
			}
		} else {
			throw new Exception( 'Unsupported database engine (' . $pCnnData['dbType'] . ').' );
		}

		return $newConnection;
	}

	//Método para abrir las conexiones.
	private function openConnection() {
		$inDate = new \DateTime('now', new \DateTimeZone('UTC'));
		$openedConnection = false;

		try {
			//Connections are Singleton.
			if( !( isset( self::$cnns[ $this->idCnn ] ) && isset( self::$cnns[ $this->idCnn ]['oCnn'] ) ) ) {
				self::$cnns[ $this->idCnn ] = array();
				self::$cnns[ $this->idCnn ]['oCnn'] = self::getNewConnection( $this->cnnData );
				self::$cnns[ $this->idCnn ]['stmt'] = null;

				$openedConnection = true;
			}

			//Begin transaction.
			$this->begin();
		} catch( \PDOException $e ) {
			throw new \ErrorsDatabaseConnection( 'Error connecting to database "' . $this->idCnn . '.' . $this->cnnData['db'] . '". (' . $e->getMessage() . ')' );
		} finally {
			if( $openedConnection ) {
				\Logger::getInstance()->registerDBTracking(array(
					'connectionID' => $this->idCnn,
					'db' => $this->cnnData['db'],
					'routine' => 'open-connection',
					'inDate' => $inDate
				));
			}
		}
	}

	//Método para obtener las conexiones existentes.
	public static function getConnections() {
		return self::$cnns;
	}

	//Método para obtener el parámetro actual.
	private function getCurrentParam( $pParams, $pCurrentValue, $pCurrentValueExtra, $pSPPrefix, $pRoutine ) {
		$currentValue = $pCurrentValue;
		$isDefined = false;
		$param = array(
			'value' => null,
			'cast' => ''
		);

		//Validamos si el valor viene definido con el prefijo o sin el prefijo.
		if( !isset( $pParams[ $currentValue ] ) ) {
			$currentValue = preg_replace( '/' . preg_quote( $pSPPrefix, '/' ) . '/', '', $pCurrentValue, 1 );
		}

		if( isset( $pParams[ $currentValue ] ) ) {
			$isDefined = true;
			$param['value'] = $pParams[ $currentValue ];
		}

		//Validamos si viene definido el parámetro.
		if( $isDefined ) {
			//Validamos que sea un valor válido.
			if( is_array( $param['value'] ) || is_object( $param['value'] ) ) {
				throw new \ErrorsDeveloper( 'The param "' . $currentValue . '" for the routine "' . $pRoutine .'" is invalid' );
			}

			$param['value'] = is_string( $param['value'] ) ? $param['value'] : ( is_bool( $param['value'] ) ? ( $param['value'] ? 1 : 0 ) : $param['value'] );

			if( $pCurrentValueExtra !== '' && $this->cnnData['dbType'] === 'PostgreSQL' ) {
				$castResponse = $this->validateCast( $pCurrentValueExtra, $currentValue, null );

				if( $castResponse ) {
					$param['cast'] = '::' . $pCurrentValueExtra;
				}
			}
		}

		return $param;
	}

	private function replaceQuotesBrackets( $pString ) {
		$pString = str_replace( '[', '', $pString );
		$pString = str_replace( ']', '', $pString );
		$pString = str_replace( '"', '', $pString );

		return $pString;
	}

	private function getSOWString( $pValue, $pType ) {
		$sltOrBy = '';
		$fieldAlias = '';
		$sltOrByArr = explode( ',', $pValue );
		$countSltOrBy = count($sltOrByArr);

		for( $x=0; $x<$countSltOrBy; $x++ ) {
			if( $sltOrBy != '' ) {
				$sltOrBy .= ', ';
			}

			$currentFieldCast = '';
			$currentField = $sltOrByArr[$x];
			$isThereAggFnc = !( strpos($currentField, '(') === false && strpos($currentField, "'") === false && strpos($currentField, ')') === false );

			// if( $isThereAggFnc === false ) {
			// 	$currentField = $this->replaceQuotesBrackets($sltOrByArr[$x]);
			// }

			//Looking for AS
			$currenFieldAsArr = explode( ' AS ', $currentField );
			$currenFieldAsArr = count($currenFieldAsArr) === 1 ? explode( ' as ', $currentField ) : $currenFieldAsArr;
			$currentFieldAlias = count($currenFieldAsArr) === 2 ? trim( $currenFieldAsArr[1] ) : '';
			$currentField = trim( $currenFieldAsArr[0] );
			
			//Looking for .
			$currenFieldPArr = explode( '.', $currentField );
			
			if( count($currenFieldPArr) === 2 ) {
				$currentField = $currenFieldPArr[1];
				$currentFieldP = $currenFieldPArr[0] . '.';
			} else {
				$currentField = $currenFieldPArr[0];
				$currentFieldP = '';
			}
			
			//Looking for ASC/DESC.
			$currenFieldAscDescArr = explode( ' ASC', $currentField );
			$currenFieldAscDescArr = count($currenFieldAscDescArr) === 1 ? explode( ' ASCENDENT ', $currentField ) : $currenFieldAscDescArr;
			$currenFieldAscDescArr = count($currenFieldAscDescArr) === 1 ? explode( ' asc', $currentField ) : $currenFieldAscDescArr;
			$currenFieldAscDescArr = count($currenFieldAscDescArr) === 1 ? explode( ' ascendent', $currentField ) : $currenFieldAscDescArr;
			
			if( count($currenFieldAscDescArr) === 2 ) {
				$currentField = $currenFieldAscDescArr[0];
				$currentFieldAscDesc = ' ASC';
			} else {
				$currenFieldAscDescArr = explode( ' DESC', $currentField );
				$currenFieldAscDescArr = count($currenFieldAscDescArr) === 1 ? explode( ' DESCENDENT ', $currentField ) : $currenFieldAscDescArr;
				$currenFieldAscDescArr = count($currenFieldAscDescArr) === 1 ? explode( ' desc', $currentField ) : $currenFieldAscDescArr;
				$currenFieldAscDescArr = count($currenFieldAscDescArr) === 1 ? explode( ' descendent', $currentField ) : $currenFieldAscDescArr;
				$currentFieldAscDesc = count($currenFieldAscDescArr) === 2 ? ' DESC' : '';
				$currentField = trim( $currenFieldAscDescArr[0] );
			}
			
			//Looking for ::
			if( $this->cnnData['dbType'] === 'PostgreSQL' ) {
				$currenFieldCastArr = explode( '::', $currentField );

				if( count($currenFieldCastArr) === 2 ) {
					$currentField = $currenFieldCastArr[0];
					$currentFieldCast = '::' . $currenFieldCastArr[1];
				} else {
					$currentField = $currenFieldCastArr[0];
					$currentFieldCast = '';
				}
			}

			if( $currentField != '*' && ( $isThereAggFnc === false || $currentFieldAlias !== '' ) ) {
				switch( $this->cnnData['dbType'] ) {
					case 'SQLServer':
						if( $isThereAggFnc === false ) {
							$currentField = '[' . $this->replaceQuotesBrackets($currentField) . ']';
						}

						if( $currentFieldAlias != '' ) {
							$currentFieldAlias = '[' . $this->replaceQuotesBrackets($currentFieldAlias) . ']';
						}
						break;
					case 'PostgreSQL':
						if( $isThereAggFnc === false ) {
							$currentField = '"' . $this->replaceQuotesBrackets($currentField) . '"';
						}

						if( $currentFieldAlias != '' ) {
							$currentFieldAlias = '"' . $this->replaceQuotesBrackets($currentFieldAlias) . '"';
						}
						break;
					default:
						if( $isThereAggFnc === false ) {
							$currentField = $this->replaceQuotesBrackets($currentField);
						}

						if( $currentFieldAlias != '' ) {
							$currentFieldAlias = $this->replaceQuotesBrackets($currentFieldAlias);
						}
						break;
				}

				if( $currentFieldAlias != '' ) {
					$currentFieldAlias = ' AS ' . $currentFieldAlias;
				}
			} /*else {
				$currentField = $sltOrByArr[$x];
			}*/
			
			$sltOrBy .= $currentFieldP . $currentField . $currentFieldCast . $currentFieldAlias . $currentFieldAscDesc;

			// if( $pType == 'orderBy' ) {
			// 	var_dump($currentFieldP, $currenFieldAscDescArr, $currentFieldAscDesc, $currentField, $sltOrBy); die();
			// }
		}

		return $sltOrBy;
	}

	//Método para ejecutar queries.
	private function execute( $pQuery, $pIUD = false, $pLog = false, $pObj = false, $pRememberTime = 0 ) {
		$isFromCache = false;
		$query = array();
		$rowCount = 0;
		$prepareQuery = null;
		$bindParams = null;
		$redisKey = null;
		$isDataInCache = false;
		$dataLog = null;

		if( is_array( $pQuery ) ) {
			$prepareQuery = $pQuery['statement'];
			$bindParams = $pQuery['bindParams'];
		} else {
			$prepareQuery = $pQuery;
		}

		if( !$pIUD && $pRememberTime ) {
			if( is_array( $pQuery ) ) {
				$redisKey = $prepareQuery . ' => ' . ( $bindParams ? json_encode( $bindParams ) : '' );
			} else {
				$redisKey = $prepareQuery;
			}
		
			$redisKey = 'lean:database:' . md5( \Helper::getSiteName() . ':' . $this->idCnn . $this->cnnData['host'] . '-' . $this->cnnData['db'] . '-' . $this->cnnData['dbType'] . '-' . $this->cnnData['schema'] . ':' . $redisKey );
			$queryCache = \Cache::get($redisKey);

			if( $queryCache ) {
				$isDataInCache = true;
				$isFromCache = true;
				$query = json_decode( $queryCache, true );
			}
		}

		if( $pLog ) {
			$dataLog = array(
				'connectionID' => $this->idCnn,
				'db' => $this->cnnData['db'],
				'schema' => $this->cnnData['schema'],
				'routine' => $prepareQuery,
				'params' => $bindParams,
				'inDate' => new \DateTime('now', new \DateTimeZone('UTC')),
				'isFromCache' => $isFromCache
			);
		}

		if( $isDataInCache === false ) {
			$this->openConnection();

			try {
				self::$cnns[ $this->idCnn ]['stmt'] = self::$cnns[ $this->idCnn ]['oCnn']->prepare( $prepareQuery );
				self::$cnns[ $this->idCnn ]['stmt']->execute( $bindParams );

				if( self::$cnns[ $this->idCnn ]['stmt'] ) {
					$rowCount = self::$cnns[ $this->idCnn ]['stmt']->rowCount();

					if( !$pIUD ) {
						if( $this->cnnData['dbType'] !== 'SQLServer' || ( $this->cnnData['dbType'] === 'SQLServer' && self::$cnns[ $this->idCnn ]['stmt']->columnCount() ) ) {
							$query = self::$cnns[ $this->idCnn ]['stmt']->fetchAll( $pObj ? PDO::FETCH_OBJ : PDO::FETCH_ASSOC );

							if( $pRememberTime ) \Cache::set($redisKey, $pRememberTime, json_encode($query, JSON_UNESCAPED_UNICODE));
						}
					}
				} else {
					throw new \ErrorsDatabase('There is an error in your SQL statement. Please check your code.');
				}
			} catch( \PDOException $e ) {
				$queryExtraData = ' -> ' . json_encode(['statement' => $prepareQuery, 'bindParams' => $bindParams]);
				//Get error.
				$messageOrig = $e->getMessage();
				$message = strtoupper( $messageOrig );
				//Get custom messages.
				$customMessages = self::$lean->getCustomMessages();
				//Custom message key.
				$cmKey = null;
				//Error type.
				$errorType = null;

				//Search custom message.
				foreach( $customMessages as $keyCM => $valueCM )  {
					foreach( $valueCM as $key => $value )  {
						if( preg_match( '/' . strtoupper( $key ) . '/i', $message ) ) {
							$errorType = $keyCM;
							$cmKey = $key;
							break;
						}
					}

					if( $cmKey ) {
						break;
					}
				}

				//Validate primary key for MySQL. MySQL do not register a name for primary
				if( $this->cnnData['dbType'] === 'MySQL' && !$errorType ) {
					if( preg_match( "/FOR KEY 'PRIMARY'/i" , $message ) && count( $customMessages['pk'] ) > 0 ) {
						foreach( $customMessages['pk'] as $key => $value )  {
							$errorType = 'pk';
							$cmKey = $key;
						}
					} else {
						throw new \ErrorsDatabase($messageOrig);
					}
				}

				if( $errorType === 'pk' ) {
					throw new \ErrorsPrimaryKey($cmKey, 'Duplicate Primary Key (' . $messageOrig . ')' . $queryExtraData);
				} else if( $errorType === 'fk' ) {
					throw new \ErrorsForeignKey($cmKey, $messageOrig . $queryExtraData);
				} else if( $errorType === 'uq' ) {
					throw new \ErrorsUniqueKey($cmKey, 'Duplicate Unique Constraint. (' . $messageOrig . ')' . $queryExtraData);
				} else {
					throw new \ErrorsDatabase( $messageOrig . $queryExtraData );
				}
			} finally {
				if( $pLog ) {
					\Logger::getInstance()->registerDBTracking($dataLog);
				}
			}
		} else if( $pLog ) {
			\Logger::getInstance()->registerDBTracking($dataLog);
		}

		return array(
			'rowCount' => $rowCount,
			'query' => $query
		);
	}

	//Método para obtener el procedimiento almacenado.
	private function getProcedure( $pSP, $pTrhowException ) {
		$sp = null;
		$querySP = array();
		$rememberTime = $this->getRememberTime(10);

		switch( $this->cnnData['dbType'] ) {
			case 'MySQL':
				$querySP = $this->execute( 'SHOW CREATE PROCEDURE ' . $pSP, false, false, false, $rememberTime )['query'];

				if( count( $querySP ) === 1 ) {
					$sp = $querySP[0]->{'Create Procedure'};
				}

				break;
			case 'PostgreSQL':
				$querySP = $this->execute("
					SELECT proname, prosrc
					FROM pg_proc
					WHERE proname = '" . $pSP . "'", false, false, false, $rememberTime)['query'];

				if( count( $querySP ) === 1 ) {
					$sp = $querySP[0]['prosrc'];
				} else if( count( $querySP ) > 1 ) {
					throw new \ErrorsDeveloper( 'The "' . $pSP . '" function is declared more than once' );
				}

				break;
			case 'SQLServer':
				$sp = '';
				$querySP = $this->execute( "SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('" . $pSP . "');", false, false, false, $rememberTime )['query'];

				if( count( $querySP ) === 1 ) {
					$querySP = $this->execute( "EXEC sp_helptext '" . $pSP . "';", false, false, false, $rememberTime )['query'];
				}

				foreach( $querySP as $key => $value ) {
					$sp .= '
						' . $value['Text'];
				}

				break;
			default:
				throw new \ErrorsDatabaseConnection( 'Unsupported database engine (' . $this->cnnData['dbType'] . ').' );
		}

		//Validamos si la rutina existe.
		if( $pTrhowException && count( $querySP ) === 0 ) {
			throw new \ErrorsDatabase('The routine "' . $pSP . '" does not exist in the database "' . $this->idCnn . '.' . $this->cnnData['db'] . '".' );
		}

		return $sp;
	}

	private function paginate( $pSP, $pSPParams, $pParams, $pIsQManager = false, $pHC = false ) {
		//Validamos que vengan definidos los parámetros correctos.
		if( !( isset( $pParams['_page'] ) && isset( $pParams['_rows'] ) ) ) {
			throw new \ErrorsDeveloper( 'Make sure to define the following variables: "_page" and "_rows"' );
		}

		$_slt = isset( $pParams['_slt'] ) ? $pParams['_slt'] : '*';
		$_page = (int)$pParams['_page'];
		$_rows = (int)$pParams['_rows'];

		if( $_page === 0 ) {
			throw new ErrorsDeveloper('The varibale "_page" must be a valid integer value');
		} else if( $_rows === 0 ) {
			throw new ErrorsDeveloper('The varibale "_rows" must be a valid integer value');
		}

		$select = '';
		$fromWhere = '';
		$sqlValuesArr = array(
			'statement' => null,
			'bindParams' => isset( $pSP['bindParams'] ) ? $pSP['bindParams'] : array()
		);
		$spPrefix = isset( $this->cnnData['spPrefix'] ) ? $this->cnnData['spPrefix'] : '';
		$rememberTime = $this->getRememberTime( isset( $pParams['_rememberTime'] ) && is_int( $pParams['_rememberTime'] ) ? $pParams['_rememberTime'] : 0 );
		$rememberTime = $rememberTime ? $rememberTime : ( self::$rememberTime ? self::$rememberTime : 0 );

		if( isset( $pParams['_spPrefix'] ) ) {
			$spPrefix = $pParams['_spPrefix'];
		}

		if( $pIsQManager ) {
			if( is_array( $pSP ) ) {
				$sp = $pSP['statement'];
			} else {
				$sp = trim( $pSP );
			}
		} else {
			//Obtenemos el procedure.
			$sp = $this->getProcedure( $pSP, true );
		}

		$arrayConsulta = explode( '-- &', $sp );

		//Validamos si la rutina trae los comentarios requeridos.
		if( count( $arrayConsulta ) === 1 ) {
			throw new \ErrorsDeveloper('It is necessary to indicate in the routine "' . $sp . '" the required comment (-- &) in order to be able to page it');
		}

		//Get "select" and "from".
		if( $pIsQManager ) {
			for( $x=0; $x<count($arrayConsulta); $x++ ) {
				$arrConsVal = trim( $arrayConsulta[$x] );

				if( $arrConsVal !== '' ) {
					if( $select === '' ) {
						$select = $arrConsVal;
					} else {
						$fromWhere = $arrConsVal;
						break;
					}
				}
			}
		} else {
			$select = $arrayConsulta[1];
			$fromWhere = $arrayConsulta[2];
		}

		//Remplazamos los parámetros.
		if( $pSPParams /*&& is_string( $pSP )*/ ) {
			foreach ( $pSPParams as $key => $value ) {
				$currentValueArray = array();
				$currentValueExtra = '';

				//Validamos si el valor es un array.
				if( is_array( $value ) ) {
					throw new \ErrorsDeveloper( 'The value "' . $key . '" must not be an array' );
				} else {
					$currentValue = is_null($value) ? $value : trim( $value );

					if( $this->cnnData['dbType'] === 'PostgreSQL' ) {
						$currentValueArray = explode( '::', $value );
						$currentValue = trim( $currentValueArray[0] );
						$currentValueExtra = count( $currentValueArray ) === 1 ? '' : trim( $currentValueArray[1] );
					}

					//Obtenemos el parámetro actual.
					$paramsArr = $this->getCurrentParam( $pParams, $currentValue, $currentValueExtra, $spPrefix, $pSP );

					//Detectamos cuántas veces se encuentra repetido este parámetro.
					//Esto se hace sólo para SQLServer porque no soporta "ATTR_EMULATE_PREPARES".
					if( $this->cnnData['dbType'] === 'SQLServer' ) {
						$currentValueAux = ( $pIsQManager ? ':' : '@' ) . $currentValue;
						$fromWhereArr = explode( $currentValueAux, $fromWhere );
						$fromWhereArrCount = sizeof( $fromWhereArr );

						if( $fromWhereArrCount > 0 ) {
							for( $x=0; $x<$fromWhereArrCount - 2; $x++ ) {
								$newParam = 'leanParamNameChange' . rand(0,1000000);
								$fromWhere = preg_replace( '/' . preg_quote( $currentValueAux, '/' ) . '/', $newParam, $fromWhere, 1 );
								//Reemplazamos.
								$fromWhere = str_replace( $newParam, ':' . $newParam . $paramsArr['cast'], $fromWhere );
								$sqlValuesArr['bindParams'][$newParam] = $paramsArr['value'];
							}
						}

						//Se cambian todos los parámetros que se repiten excepto el úlitmo.
						//Por eso se necesita contemplar el parámetro de manera original.
						$sqlValuesArr['bindParams'][$currentValue] = $paramsArr['value'];
						$fromWhere = str_replace( $currentValueAux, ':' . $currentValue . $paramsArr['cast'], $fromWhere );
						//Quitamos el arrova de los parámetros.
						$fromWhere = str_replace( '@:', ':', $fromWhere );
					} else {
						//Validar que exista el parámetro.
						$paramCount = sizeof( explode( $currentValue, $fromWhere ) ) - 1;

						if( $paramCount > 0 ) {
							//Reemplazamos.
							$bindParameter = ( $pHC === false ? ':' : '' ) . $currentValue;
							$fromWhere = str_replace( $currentValue, $bindParameter . $paramsArr['cast'], $fromWhere );
							$sqlValuesArr['bindParams'][$currentValue] = $paramsArr['value'];

							//Quitamos las dobles comillas en caso de ser PostgreSQL.
							if( $pHC === false && $this->cnnData['dbType'] === 'PostgreSQL' ) {
								$fromWhere = str_replace( '"' . $bindParameter . '"', $bindParameter, $fromWhere );
							}
						}
					}
				}
			}
		}

		$fromWhere = str_replace( ';', '', $fromWhere );
		$idCacheCounter = md5( $this->idCnn . '_' . $select . '_' . $fromWhere . json_encode($sqlValuesArr['bindParams']) );
		$totalPages = 0;
		$totalRows = 0;

		//Obtenemos el total de registros.
		if( isset( $this->totalRowsCache[ $idCacheCounter ] ) ) {
			$totalRows = $this->totalRowsCache[ $idCacheCounter ];
		} else {
			$sqlValuesArr['statement'] = 'SELECT COUNT(*) AS total_rows ' . $fromWhere;
			$query = $this->execute( $sqlValuesArr, false, true, false, $rememberTime )['query'];
			$this->totalRowsCache[ $idCacheCounter ] = $totalRows = isset( $query[0]['total_rows'] ) ? $query[0]['total_rows'] : 0;
		}

		if( $totalRows > 0 ) {
			$totalPages = ceil( $totalRows / $_rows );
		}

		if( isset( $pParams['_onlyCount'] ) && $pParams['_onlyCount'] === true ) {
			return array(
				'pageData' => null,
				'page' => $_page,
				'pages' => $totalPages,
				'totalRows' => $totalRows
			);
		}

		$start = $_rows * $_page - $_rows;
		$pagShowExec = array();
		$varPR = null;

		//Validamos que _page y _rows sea mayor a cero.
		if( $_page === 0 ) {
			$varPR = '_page';
		} else if( $_rows === 0 ) {
			$varPR = '_rows';
		}

		if( $varPR ) {
			throw new \ErrorsDeveloper( 'The variable "' . $varPR . '" must be greater than zero' );
		}

		if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
			$pagShowExec['statement1'] = $sqlValuesArr['statement'];
		}

		$_select = $this->getSOWString( $_slt, 'select' );
		$_orderBy = $this->getSOWString( isset( $pParams['_orderBy'] ) ? $pParams['_orderBy'] : explode( ',', $_select )[0], 'orderBy' );

		if( $_orderBy === '*' ) throw new \ErrorsDeveloper( 'You must to define the variable "_orderBy"' );
		
		//Obtenemos sólo los registros que requiere el paginado.
		if( $this->cnnData['dbType'] === 'SQLServer' ) {
			$sqlValuesArr['statement'] = "
				SELECT " . $_select . "
				FROM(
					" . $select . " " .
					$fromWhere . "
					ORDER BY " . $_orderBy . "
					OFFSET " . $start . " ROWS FETCH NEXT " . $_rows . " ROWS ONLY
				) tmp";
		} else {
			$sqlValuesArr['statement'] = '
				SELECT ' . $_select . '
				FROM(
					' . $select . ' ' .
					$fromWhere . '
					ORDER BY ' . $_orderBy .
					(
						$this->cnnData['dbType'] === 'MySQL'
							?
								' LIMIT ' . $start . ', ' . $_rows
							:
								' LIMIT ' . $_rows . ' OFFSET ' . $start
					) . '
				) tmp';
		}

		$sqlValuesArr['statement'] = implode("\n", array_map('trim', explode("\n", $sqlValuesArr['statement'])));

		if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
			$pagShowExec['statement2'] = $sqlValuesArr['statement'];
			$pagShowExec['bindParams'] = $sqlValuesArr['bindParams'];

			var_dump( $pagShowExec );
			die();
		}

		//Ejecutamos la consulta.
		$query = $this->execute( $sqlValuesArr, false, true, false, $rememberTime )['query'];

		return array(
			'pageData' => $query,
			'page' => $_page,
			'pages' => $totalPages,
			'totalRows' => $totalRows
		);
	}

	public function exec( $pSP = null, $pParams = null, $pHC = false ) {
		$queryResult = null;
		$paginate = isset( $pParams['_paginate'] ) && $pParams['_paginate'];
		$cast = isset( $pParams['_cast'] ) ? $pParams['_cast'] : null;
		$rememberTime = $this->getRememberTime( isset( $pParams['_rememberTime'] ) && is_int( $pParams['_rememberTime'] ) ? $pParams['_rememberTime'] : 0 );
		$rememberTime = $rememberTime ? $rememberTime : ( self::$rememberTime ? self::$rememberTime : 0 );

		if( is_string($pSP) ) $pSP = trim($pSP);

		//Si los parámetros vienen NULL, significa que usaremos el QManager.
		//Validamos si se va a ejecutar código duro.
		if( $pHC === true ) {
			$sqlValuesArr = array(
				'statement' => $pSP,
				'bindParams' => array()
			);
			$HCKeysParams = array();

			//Obtenemos sólo los parámetros que se van a bindear.
			if( $pParams && is_array($pParams) ) {
				foreach( $pParams as $key => $value ) {
					$boundParams = explode( ':' . $key, $pSP );

					if( count( $boundParams ) > 1 ) {
						$sqlValuesArr['bindParams'][ $key ] = $value;
						array_push( $HCKeysParams, $key );
					}
				}
			}

			//Validamos si el query es para un paginado.
			if( $paginate ) {
				return $this->paginate( $sqlValuesArr, $HCKeysParams, $pParams, true, true );
			}

			if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
				var_dump( $sqlValuesArr );
				die();
			}

			$queryResult = $this->execute( $sqlValuesArr, false, true, false, $rememberTime )['query'];
		} else if( !is_null( $pSP ) && !is_null( $pParams ) ) { //Llamado a store procedure o función.
			$spArr = explode( '(', $pSP );
			$pSP = $spArr[0];
			$spParams = count( $spArr ) === 2 ? $spArr[1] : null; //Validamos si el programador definió los parámetros.
			$spParamsAux = array();
			$params = '';
			$count = 1;
			$total = 0;
			$spPrefix = isset( $this->cnnData['spPrefix'] ) ? $this->cnnData['spPrefix'] : '';

			if( isset( $pParams['_spPrefix'] ) ) {
				$spPrefix = $pParams['_spPrefix'];
			}

			//Obtenemos los parámetros del store.
			//Validate cache.
			if( isset( self::$cache[ $this->idCnn ][ $pSP ] ) && is_null( $spParams ) ) {
				$spParams = self::$cache[ $this->idCnn ][ $pSP ];
			} else if( is_null( $spParams ) ) {
				//Primero, validamos si existe el sp de lean.
				$sp = $this->getProcedure( 'lean_get_params', false );
				$rememberTime2 = $this->getRememberTime(3600);

				//Segundo, validamos si existe el sp de la consulta.
				$this->getProcedure( $pSP, true );

				switch( $this->cnnData['dbType'] ) {
					case 'MySQL':
						if( $sp ) {
							$spParams = $this->execute( "CALL lean_get_params( '" . $pSP . "' )", false, false, false, $rememberTime2 )['query'];
						} else {
							$spParams = $this->execute("
								SELECT param_list
								FROM mysql.proc
								WHERE db = '" . $this->cnnData['db'] . "'
								AND name = '" . $pSP . "';
							", false, true, false, $rememberTime2)['query'];
						}

						$spParams = $spParams[0]['param_list'];
						$spParams = str_replace( array( '`' ), array( '' ), $spParams );
						$spParams = explode( ',', $spParams );

						break;

					case 'PostgreSQL':
						if( $sp ) {
							$spParams = $this->execute( "SELECT * FROM lean_get_params( '" . $pSP . "' )", false, false, false, $rememberTime2 )['query'];
						} else {
							$spParams = $this->execute("
								SELECT pg_catalog.pg_get_function_identity_arguments( p.oid ) AS param_list
								FROM pg_catalog.pg_proc p

								INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace

								WHERE n.nspname = 'public'
								AND proname = '" . $pSP . "';
							", false, false, false, $rememberTime2)['query'];
						}

						$spParams = $spParams[0]['param_list'];
						$spParams = str_replace( '"', '', $spParams );
						$spParams = str_replace( array( '{', '}' ), array( '', '' ), $spParams );
						$spParams = explode( ',', $spParams );

						break;

					case 'SQLServer':
						if( $sp ) {
							$spParams = $this->execute( "EXECUTE lean_get_params '" . $pSP . "'", false, false, false, $rememberTime2 )['query'];
						} else {
							$spParams = $this->execute("
								SELECT REPLACE( name, '@', '' ) + ' ' + TYPE_NAME(user_type_id) AS param_list
								FROM sys.parameters
								WHERE OBJECT_ID = OBJECT_ID( '" . $pSP . "' );
							", false, false, false, $rememberTime2)['query'];
						}

						break;
				}

				//Add to cache.
				self::$cache[ $this->idCnn ][ $pSP ] = $spParams;
			} else { //Significa que el programador definió los parámetros del sp o función.
				$spParams = str_replace( ')', '', $spParams );
				$spParams = explode( ',', $spParams );
			}

			//Validamos si el query es para un grid.
			if( $paginate ) {
				foreach( $spParams as $key => $value ) {
					//Si viene como un objeto, significa que hay que obtener el valor del atributo param_list. (Esto pasa con SQLServer)
					if( is_array( $value ) && $this->cnnData['dbType'] === 'SQLServer' ) {
						$value = $value['param_list'];
					}

					$valueAux = trim( $value );

					if( $valueAux !== '' ) {
						$arrayValue = explode( ' ', $valueAux );
						array_push( $spParamsAux, $arrayValue[0] );
					}
				}

				return $this->paginate( $pSP, $spParamsAux, $pParams, false, false );
			} else {
				$total = count( $spParams );
				$sqlValuesArr = array(
					'statement' => null,
					'bindParams' => array()
				);

				//Armamos los parámetros.
				foreach( $spParams as $key => $value ) {
					//Si viene como un objeto, significa que hay que obtener el valor del atributo param_list. (Esto pasa con SQLServer)
					if( is_array( $value ) && $this->cnnData['dbType'] === 'SQLServer' ) {
						$value = $value['param_list'];
					}

					$value = trim( $value );

					if( $value !== '' ) {
						$currentValueArray = explode( ' ', $value );
						$currentValue = trim( $currentValueArray[0] );
						$currentValueExtra = count( $currentValueArray ) > 1 ? trim( $currentValueArray[1] ) : '';

						//Si no hay casteo, validamos si el programador estableció un casteo.
						if( !$currentValueExtra )  {
							$currentValueArray = explode( '::', $value );
							$currentValue = trim( $currentValueArray[0] );
							$currentValueExtra = count( $currentValueArray ) === 1 ? '' : trim( $currentValueArray[1] );
						}

						if( $this->cnnData['dbType'] === 'PostgreSQL' && strtoupper( $currentValueExtra ) === 'CHARACTER' ) {
							$currentValueExtra = 'VARCHAR';
						}

						//Obtenemos el parámetro actual.
						$paramsArr = $this->getCurrentParam( $pParams, $currentValue, $currentValueExtra, $spPrefix, $pSP );
						$sqlValuesArr['bindParams'][$currentValue] = $paramsArr['value'];
						$params .= ':' . $currentValue . $paramsArr['cast'];

						if( $count < $total )
							$params .= ', ';

						$count++;
					}
				}

				//Ejecutamos.
				$exec = null;
				switch( $this->cnnData['dbType'] ) {
					case 'MySQL':
						$exec = 'CALL ';
						break;
					case 'PostgreSQL':
						$exec = 'SELECT * FROM ' . $this->cnnData['schema'] . '.';
						break;
					case 'SQLServer':
						$exec = 'EXECUTE ';
						break;
				}

				if( $this->cnnData['dbType'] === 'SQLServer' ) {
					$sqlValuesArr['statement'] = $exec . $pSP . ' ' . $params;
				} else {
					$sqlValuesArr['statement'] = $exec . $pSP . '( ' . $params . ' )';
				}

				if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
					var_dump( $sqlValuesArr );
					die();
				}

				$queryResult = $this->execute( $sqlValuesArr, false, true, false, $rememberTime )['query'];
			}
		} else {
			$paginate = isset( $pSP['_paginate'] ) && $pSP['_paginate'];
			$cast = isset( $pSP['_cast'] ) ? $pSP['_cast'] : null;
			$query = $this->qManager( $pSP );
			$rememberTime = $this->getRememberTime( isset( $pSP['_rememberTime'] ) && is_int( $pSP['_rememberTime'] ) ? $pSP['_rememberTime'] : 0 );
			$rememberTime = $rememberTime ? $rememberTime : ( self::$rememberTime ? self::$rememberTime : 0 );

			if( isset( $pSP['_showExec'] ) && $pSP['_showExec'] === true ) {
				var_dump($query);
				die();
			}

			//Validamos si el query es para un paginado.
			if( $paginate ) {
				$queryResult = $this->paginate( $query, null, $pSP, true, false );
			} else {
				$queryResult = $this->execute( $query, false, true, false, $rememberTime )['query'];
			}
		}

		//Validamos si debemos forzar algún casteo.
		if( $cast ) {
			$queryResultAux = $paginate ? $queryResult['pageData'] : $queryResult;
			$queryResultAux = $this->castQueryResult( $queryResultAux, $cast );

			if( $paginate ) {
				$queryResult['pageData'] = $queryResultAux;
			} else {
				$queryResult = $queryResultAux;
			}
		}

		self::$rememberTime = 0;

		return $queryResult;
	}

	public function getOne( $pSP = null, $pParams = null, $pHC = false ) {
		$paginate = isset( $pSP['_paginate'] ) && $pSP['_paginate'];
		$query = $this->exec( $pSP, $pParams, $pHC );

		if( $paginate ) {
			$count = count($query['pageData']);
		} else {
			$count = count($query);
		}

		if( $count !== 1 ) {
			throw new \ErrorsDeveloper( 'Query result expects one row | ' . ( is_array($pSP) ? json_encode($pSP) : $pSP ) );
		}

		if( !$paginate ) {
			$query = $query[0];
		}

		return $query;
	}

	public function statement( $pStmt, $pParams = null ) {
		$query = $this->exec( $pStmt, $pParams, true );

		return $query;
	}

	private function castQueryResult( $pQuery, $pCast ) {
		if( $pCast ) {
			$countQR = count($pQuery);

			for( $x=0; $x<$countQR; $x++ ) {
				foreach( $pQuery[$x] as $qrKey => &$qrValue ) {
					if( isset( $pCast[$qrKey] ) ) {
						switch( strtolower( $pCast[$qrKey] ) ) {
							case 'int':
							case 'integer':
							case 'numeric':
							case 'number':
								$qrValue = is_null($qrValue) ? null : (int)$qrValue;
								break;
							case 'float':
							case 'double':
								$qrValue = is_null($qrValue) ? null : (float)$qrValue;
								break;
							case 'bool':
							case 'boolean':
								$qrValue = is_null($qrValue) ? null : (bool)$qrValue;
								break;
						}
					}
				}
			}
		}

		return $pQuery;
	}

	//Método para hacer un insert.
	public function insert( $pTable = null, $pParams = null ) {
		//Validate if $pTable and "$pParams" are defined.
		if( is_null( $pTable ) || is_null( $pParams ) ) {
			throw new \ErrorsDeveloper( '(insert) Be sure to send the table name and required parameters' );
		}

		//Obtenemos la tabla y el primary key consecutivo.
		$table = str_replace( '[', '', $pTable );
		$table = str_replace( ']', '', $table );
		$table = str_replace( '"', '', $table );
		$tableArray = explode( ':', $table );
		$table = $tableForInsert = trim( $tableArray[0] );
		$primaryKeyConsecutivo = $primaryKeyConsecutivoForInsert = count( $tableArray ) === 2 ? trim( $tableArray[1] ) : null;

		switch( $this->cnnData['dbType'] ) {
			case 'SQLServer':
				$tableForInsert = '[' . $tableForInsert . ']';
				$primaryKeyConsecutivoForInsert = '[' . $primaryKeyConsecutivoForInsert . ']';
				break;
			case 'PostgreSQL':
				$tableForInsert = '"' . $tableForInsert . '"';
				$primaryKeyConsecutivoForInsert = '"' . $primaryKeyConsecutivoForInsert . '"';
				break;
			default:
				$tableForInsert = $tableForInsert;
				$primaryKeyConsecutivoForInsert = $primaryKeyConsecutivoForInsert;
				break;
		}

		$sqlWhere = '';
		$sqlNextID = '';
		$sqlInsert = 'INSERT INTO ' . $tableForInsert . '( ';
		$sqlValues = 'VALUES';
		$sqlValuesArr = array(
			'statement' => null,
			'bindParams' => array()
		);
		$castDev = array();
		$castFw = array();
		$message = '(insert on table "' . $table . '") ';
		$nextID = array();
		$inserts = null;

		if( isset( $pParams['_cast'] ) ) {
			$castDev = $pParams['_cast'];
		} else {
			$castFw = $this->getColumnsToCast( $table );
		}

		//Validamos que venga definida la variable "_insert".
		if( !isset( $pParams['_insert'] ) ) {
			$pParams['_insert'] = $this->createArrayISW( $table, '_insert', $pParams );
		}

		$_insert = $pParams['_insert'];

		//Si se desea insertar varios registros y la tabla cuenta con un consecutivo manual, entonces no será posible.
		//Sólo se puede con tablas que tengan tipo de dato SERIAL o IDENTITY.
		if( isset( $_insert[0] ) && !is_null( $primaryKeyConsecutivo ) ) {
			throw new \ErrorsDeveloper( '(insert) You can only use multi insert for tables that have primary key as SERIAL or IDENTITY' );
		}

		//Validamos si es un multi-insert.
		$inserts = !isset( $_insert[0] ) ? array( $_insert ) : $_insert;
		$insertCount = count( $inserts );

		//Validamos si debemos parsear un JSON.
		if( !is_array( $_insert ) ) {
			$_insert = json_decode( $_insert, true );

			if( is_null( $_insert ) ) {
				throw new \ErrorsDeveloper( $message . 'The "_insert" variable is not a correct JSON format' );
			}
		}

		if( count( $_insert ) === 0 ) {
			throw new \ErrorsDeveloper( $message . 'The "_insert" variable should not be empty' );
		}

		if( !is_null( $primaryKeyConsecutivo ) ) {
			//Validate cache.
			if( isset( self::$cache[ $this->idCnn ][ $table ]['primaryKey'] ) ) {
				$queryPrimaryKey = self::$cache[ $this->idCnn ][ $table ]['primaryKey'];
			} else {
				$rememberTime = $this->getRememberTime(3600);
				//Obtenemos los primary key de la tabla.
				//Primero, validamos si existe el sp.
				$sp = $this->getProcedure( 'lean_get_primary_key', false );

				if( $sp ) {
					$exec = null;
					$sp = 'lean_get_primary_key';
					$params = "'" . $this->cnnData['db'] . "', '" . $table . "'";

					switch( $this->cnnData['dbType'] ) {
						case 'MySQL':
							$exec = 'CALL ';
							break;
						case 'PostgreSQL':
							$exec = 'SELECT * FROM ';
							break;
						case 'SQLServer':
							$exec = 'EXECUTE ';
							break;
					}

					if( $this->cnnData['dbType'] === 'SQLServer' ) {
						$exec = $exec . $sp . ' ' . $params;
					} else {
						$exec = $exec . $sp . '( ' . $params . ' )';
					}

					$queryPrimaryKey = $this->execute( $exec, false, false, false, $rememberTime )['query'];
				} else {
					$queryPrimaryKey = $this->execute("
						SELECT c.column_name
						FROM information_schema.columns c

						INNER JOIN information_schema.key_column_usage kcu
							ON c.table_catalog = kcu.constraint_catalog
							AND c.table_name = kcu.table_name
							AND c.column_name = kcu.column_name

						INNER JOIN information_schema.table_constraints tc
							ON kcu.constraint_catalog = tc.constraint_catalog
							AND kcu.table_name = tc.table_name
							AND kcu.constraint_name = tc.constraint_name

						WHERE (
							( '" . $this->cnnData['dbType'] . "' = 'MySQL' AND c.table_schema = '" . $this->cnnData['db'] . "' ) OR
							( ( '" . $this->cnnData['dbType'] . "' = 'PostgreSQL' OR '" . $this->cnnData['dbType'] . "' = 'SQLServer' ) AND c.table_catalog = '" . $this->cnnData['db'] . "' )
						)
						AND c.table_name = '" . $table . "'
						AND tc.constraint_type = 'PRIMARY KEY'

						ORDER BY c.ordinal_position
					", false, false, false, $rememberTime)['query'];
				}

				//Add to cache.
				self::$cache[ $this->idCnn ][ $table ]['primaryKey'] = $queryPrimaryKey;
			}

			//Armamos el where para el primary key consecutivo.
			for( $x=0; $x<$insertCount; $x++ ) {
				$cont = 0;
				$sqlWhere = '';

				foreach( $queryPrimaryKey as $row ) {
					if( strtoupper( $primaryKeyConsecutivo ) != strtoupper( $row['column_name'] ) ) {
						if( $cont === 0 ) {
							$sqlWhere .= 'WHERE ';
						} else {
							$sqlWhere .= '
							AND ';
						}

						//Validamos si está definido este campo.
						if( !isset( $_insert[ $row['column_name'] ] ) ) {
							throw new \ErrorsDeveloper( $message . 'The "' . $row['column_name'] . '" property is not defined in the parameters' );
						}

						$value = $_insert[ $row['column_name'] ];
						//$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : trim( $value ) );
						$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : $value );
						//Validamos si este campo se debe castear.
						$castVal = $this->getCast( ':', $row['column_name'], $castDev, $castFw );

						switch( $this->cnnData['dbType'] ) {
							case 'SQLServer':
								$sqlWhere .= '[' . $row['column_name'] . '] ';
								break;
							case 'PostgreSQL':
								$sqlWhere .= '"' . $row['column_name'] . '" ';
								break;
							default:
								$sqlWhere .= $row['column_name'] . ' ';
								break;
						}

						//Validamos forma de casteo.
						/*if( $this->cnnData['dbType'] === 'PostgreSQL' || $castVal === '' ) {
							$sqlWhere .= ' = :' . $row['column_name'] . $castVal;
						} else {
							$sqlWhere .= ' = ' . $castVal;
						}*/
						$sqlWhere .= ' = ' . $castVal;
						$sqlValuesArr['bindParams'][ $row['column_name'] ] = $currentValue;
						$cont++;
					}
				}

				//Obtenemos el next id.
				$sqlValuesArr['statement'] = '
					SELECT COALESCE( MAX( ' . $primaryKeyConsecutivoForInsert . ' ), 0 ) + 1 AS ' . $primaryKeyConsecutivoForInsert . '
					FROM ' . $tableForInsert . '
					' . $sqlWhere . '
				';
				$queryNextID = $this->execute( $sqlValuesArr, false, true, false, 0 )['query'];

				//Armamos el insert del campo del primary key conescutivo.
				switch( $this->cnnData['dbType'] ) {
					case 'SQLServer':
						$sqlInsert .= '[' . $primaryKeyConsecutivo . ']';
						break;
					case 'PostgreSQL':
						$sqlInsert .= '"' . $primaryKeyConsecutivo . '"';
						break;
					default:
						$sqlInsert .= $primaryKeyConsecutivo;
						break;
				}

				$sqlInsert .= ', ';
				array_push( $nextID, $queryNextID[0][$primaryKeyConsecutivo] );
			}

			$sqlValuesArr['statement'] = null;
			$sqlValuesArr['bindParams'] = array();
		}

		//Armamos el insert de los demás campos.
		for( $x=0; $x<$insertCount; $x++ ) {
			$_insert = $inserts[$x];
			$count = 0;

			if( $x === 0 ) {
				$sqlValues .= '( ';
			} else {
				$sqlValues .= ', ( ';
			}

			//Consecutivo.
			if( !is_null( $primaryKeyConsecutivo ) ) {
				$sqlValues .= ':' . $x . '_' . $primaryKeyConsecutivo . ', ';
				$sqlValuesArr['bindParams'][ $x . '_' . $primaryKeyConsecutivo ] = $nextID[$x];
			}

			foreach( $_insert as $key => $value ) {
				$key = is_string( $key ) ? trim( $key ) : $key;

				//Validamos que el valor no sea un array.
				if( is_array( $value ) ) {
					throw new \ErrorsDeveloper(  $message . 'The "_insert" variable can not have nested values ​​of type "array"' );
				}

				//Obtenemos el valor actual.
				//Se debe validar si el valor viene NULL.
				//$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : trim( $value ) );
				$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : $value );
				//Validamos si este campo se debe castear.
				$castVal = $this->getCast( ':' . $x . '_', $key, $castDev, $castFw );

				if( $count > 0 ) {
					$sqlValues .= ', ';

					if( $x === 0 ) {
						$sqlInsert .= ', ';
					}
				}

				if( $x === 0 ) {
					switch( $this->cnnData['dbType'] ) {
						case 'SQLServer':
							$sqlInsert .= '[' . $key . ']';
							break;
						case 'PostgreSQL':
							$sqlInsert .= '"' . $key . '"';
							break;
						default:
							$sqlInsert .= $key;
							break;
					}
				}

				if( is_string( $key ) ) {
					$key = str_replace( '"', '', $key );
					$key = str_replace( '-', '_', $key );
				}

				//Validamos forma de casteo.
				/*if( $this->cnnData['dbType'] === 'PostgreSQL' || $castVal === '' ) {
					$sqlValues .= ':' . $x . '_' . $key . $castVal;
				} else {
					$sqlValues .= $castVal;
				}*/
				$sqlValues .= $castVal;
				$sqlValuesArr['bindParams'][ $x . '_' . $key ] = $currentValue;
				$count++;
			}

			$sqlValues .= ' )';
		}

		$sqlValuesArr['statement'] =
			$sqlInsert . ' )
			' . $sqlValues . ';';

		if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
			var_dump( $sqlValuesArr );
			die();
		}

		//Ejecutamos el script.
		$this->execute( $sqlValuesArr, true, true )['query'];

		//Validamos si existe un identity.
		if( is_null( $primaryKeyConsecutivo ) ) {
			$rememberTime = $this->getRememberTime(3600);

			if( $this->cnnData['dbType'] === 'SQLServer' ) {
				$queryIdentity = $this->execute("
					SELECT NAME AS column_name
					FROM SYS.IDENTITY_COLUMNS
					WHERE OBJECT_NAME(OBJECT_ID) = '" . $table . "'
				", false, false, false, $rememberTime)['query'];

				if( $queryIdentity ) {
					$primaryKeyConsecutivo = $queryIdentity[0]['column_name'];
					$queryNextID = $this->execute("
						SELECT @@IDENTITY AS " . $primaryKeyConsecutivo . "
					")['query'];
				}
			} else if( $this->cnnData['dbType'] === 'PostgreSQL' ) {
				$queryIdentity = $this->execute("
					SELECT c.column_name, pg_get_serial_sequence( '\"$table\"', c.column_name ) AS sequence_name
					FROM information_schema.columns c

					INNER JOIN information_schema.key_column_usage kcu
						ON c.table_catalog = kcu.constraint_catalog
						AND c.table_name = kcu.table_name
						AND c.column_name = kcu.column_name

					INNER JOIN information_schema.table_constraints tc
						ON kcu.constraint_catalog = tc.constraint_catalog
						AND kcu.table_name = tc.table_name
						AND kcu.constraint_name = tc.constraint_name

					WHERE (
						( '" . $this->cnnData['dbType'] . "' = 'MySQL' AND c.table_schema = '" . $this->cnnData['db'] . "' ) OR
						( ( '" . $this->cnnData['dbType'] . "' = 'PostgreSQL' OR '" . $this->cnnData['dbType'] . "' = 'SQLServer' ) AND c.table_catalog = '" . $this->cnnData['db'] . "' )
					)
					AND c.table_name = '" . $table . "'
					AND tc.constraint_type = 'PRIMARY KEY'
					AND pg_get_serial_sequence( '\"$table\"', c.column_name ) IS NOT NULL
				", false, false, false, $rememberTime)['query'];

				if( $queryIdentity ) {
					$primaryKeyConsecutivo = $queryIdentity[0]['column_name'];
					$queryNextID = $this->execute('SELECT last_value AS "' . $primaryKeyConsecutivo . '" FROM ' . $queryIdentity[0]['sequence_name'])['query'];
				}
			}
		}

		if( !is_null( $primaryKeyConsecutivo ) ) {
			return $queryNextID[0][$primaryKeyConsecutivo];
		}
	}

	public function updateOne( $pTable = null, $pParams = null ) {
		$rowCount = $this->update( $pTable, $pParams );

		if( $rowCount !== 1 ) {
			throw new \ErrorsDeveloper( 'Update on table ' . $pTable . ' affected ' . $rowCount . ' row(s)' );
		}
	}

	public function update( $pTable = null, $pParams = null ) {
		//Validate if $pTable and "$pParams" are defined.
		if( is_null( $pTable ) || is_null( $pParams ) ) {
			throw new \ErrorsDeveloper( '(update) Be sure to send the table name and required parameters' );
		}

		$message = '(update on table ' . $pTable . ') ';

		//Obtenemos la tabla.
		$table = str_replace( '[', '', trim($pTable) );
		$table = str_replace( ']', '', $table );
		$table = str_replace( '"', '', $table );
		$tableForUpdate = $table;
		$sqlSet = '
			SET ';
		$sqlWhere = '
			WHERE ';
		$sqlValuesArr = array(
			'statement' => null,
			'bindParams' => array()
		);
		$castDev = array();
		$castFw = array();

		switch( $this->cnnData['dbType'] ) {
			case 'SQLServer':
				$tableForUpdate = '[' . $tableForUpdate . ']';
				break;
			case 'PostgreSQL':
				$tableForUpdate = '"' . $tableForUpdate . '"';
				break;
			default:
				$tableForUpdate = $tableForUpdate;
				break;
		}

		//Casteos.
		if( isset( $pParams['_cast'] ) ) {
			$castDev = $pParams['_cast'];
		} else {
			$castFw = $this->getColumnsToCast( $table );
		}

		//Validamos que venga definida la variable "_set".
		if( !isset( $pParams['_set'] ) ) {
			$pParams['_set'] = $this->createArrayISW( $table, '_set', $pParams );
		}

		$_set = $pParams['_set'];

		//Validamos si debemos parsear un JSON.
		if( !is_array( $_set ) ) {
			$_set = json_decode( $_set, true );

			if( is_null( $_set ) ) {
				throw new \ErrorsDeveloper( $message . 'The "_set" variable is not a correct JSON format' );
			}
		}

		//Armamos el SET.
		$cont = 0;
		foreach( $_set AS $key => $value ) {
			$key = is_string( $key ) ? trim( $key ) : $key;

			//Validamos que el valor no sea un array.
			if( is_array( $value ) ) {
				throw new \ErrorsDeveloper(  $message . 'The "_set" variable can not have nested values ​​of type "array"' );
			}

			//Se debe validar si el valor viene NULL.
			//$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : trim( $value ) );
			$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : $value );
			//Validamos si este campo se debe castear.
			$castVal = $this->getCast( ':s_', $key, $castDev, $castFw );

			if( $cont > 0 ) {
				$sqlSet .= ',
				';
			}

			switch( $this->cnnData['dbType'] ) {
				case 'SQLServer':
					$sqlSet .= '[' . $key . ']';
					break;
				case 'PostgreSQL':
					$sqlSet .= '"' . $key . '"';
					break;
				default:
					$sqlSet .= $key;
					break;
			}

			//Validamos forma de casteo.
			/*if( $this->cnnData['dbType'] === 'PostgreSQL' || $castVal === '' ) {
				$sqlSet .= ' = :s_' . $key . $castVal;
			} else {
				$sqlSet .= ' = ' . $castVal;
			}*/
			$sqlSet .= ' = ' . $castVal;
			$sqlValuesArr['bindParams'][ 's_' . $key ] = $currentValue;
			$cont++;
		}

		//Validamos que venga definida la variable "where".
		if( !isset( $pParams['_where'] ) ) {
			$pParams['_where'] = $this->createArrayISW( $table, '_where', $pParams );
		}

		$_where = $pParams['_where'];

		//Validamos si debemos parsear un JSON.
		if( !is_array( $_where ) ) {
			$_where = json_decode( $_where, true );

			if( is_null( $_where ) ) {
				throw new \ErrorsDeveloper( $message . 'The "_where" variable is not a correct JSON format' );
			}
		}

		//Armamos el WHERE.
		$cont = 0;
		foreach( $_where AS $key => $value ) {
			$key = is_string( $key ) ? trim( $key ) : $key;

			//Validamos que el valor no sea un array.
			if( is_array( $value ) ) {
				throw new \ErrorsDeveloper(  $message . 'The "_where" variable can not have nested values ​​of type "array"' );
			}

			//$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : trim( $value ) );
			$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : $value );
			//Validamos si este campo se debe castear.
			$castVal = $this->getCast( ':w_', $key, $castDev, $castFw );

			if( $cont > 0 ) {
				$sqlWhere .= '
					AND ';
			}

			//Si la posición es numérica o posición vacía pondremos tal como viene el valor.
			if( is_int($key) || $key === '' ) {
				$sqlWhere .= $value;
				continue;
			}

			switch( $this->cnnData['dbType'] ) {
				case 'SQLServer':
					$sqlWhere .= '[' . $key . ']';
					break;
				case 'PostgreSQL':
					$sqlWhere .= '"' . $key . '"';
					break;
				default:
					$sqlWhere .= $key;
					break;
			}

			//Validamos forma de casteo.
			/*if( $this->cnnData['dbType'] === 'PostgreSQL' || $castVal === '' ) {
				$sqlWhere .= ' = :w_' . $key . $castVal;
			} else {
				$sqlWhere .= ' = ' . $castVal;
			}*/
			$sqlWhere .= ' = ' . $castVal;
			$sqlValuesArr['bindParams'][ 'w_' . $key ] = $currentValue;
			$cont++;
		}

		$sqlValuesArr['statement'] =
			'UPDATE ' . $tableForUpdate
				. $sqlSet
				. $sqlWhere;

		if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
			var_dump( $sqlValuesArr );
			die();
		}

		//Ejecutamos el script.
		return $this->execute( $sqlValuesArr, true, true )['rowCount'];
	}

	public function deleteOne( $pTable = null, $pParams = null ) {
		$rowCount = $this->delete( $pTable, $pParams );

		if( $rowCount !== 1 ) {
			throw new \ErrorsDeveloper( 'Delete on table ' . $pTable . ' affected ' . $rowCount . ' row(s)' );
		}
	}

	public function delete( $pTable = null, $pParams = null ) {
		//Validate if $pTable and "$pParams" are defined.
		if( is_null( $pTable ) || is_null( $pParams ) ) {
			throw new \ErrorsDeveloper( '(delete) Be sure to send the table name and required parameters' );
		}

		$message = '(delete on table ' . $pTable . ') ';

		//Obtenemos la tabla.
		$table = $tableForDelete = trim( $pTable );
		$sqlWhere = '
			WHERE ';
		$sqlValuesArr = array(
			'statement' => null,
			'bindParams' => array()
		);
		$castDev = array();
		$castFw = array();

		switch( $this->cnnData['dbType'] ) {
			case 'SQLServer':
				$tableForDelete = '[' . $tableForDelete . ']';
				break;
			case 'PostgreSQL':
				$tableForDelete = '"' . $tableForDelete . '"';
				break;
			default:
				$tableForDelete = $tableForDelete;
				break;
		}

		if( isset( $pParams['_cast'] ) ) {
			$castDev = $pParams['_cast'];
		} else {
			$castFw = $this->getColumnsToCast( $table );
		}

		//Validamos que venga definida la variable "_where".
		if( !isset( $pParams['_where'] ) ) {
			$pParams['_where'] = $this->createArrayISW( $table, '_where', $pParams );
		}

		$_where = $pParams['_where'];

		//Validamos si debemos parsear un JSON.
		if( !is_array( $_where ) ) {
			$_where = json_decode( $_where, true );

			if( is_null( $_where ) ) {
				throw new \ErrorsDeveloper( $message . 'The "_where" variable is not a correct JSON format' );
			}
		}

		//Armamos el WHERE.
		$cont = 0;
		foreach( $_where AS $key => $value ) {
			$key = is_string( $key ) ? trim( $key ) : $key;

			//Validamos que el valor no sea un array.
			if( is_array( $value ) ) {
				throw new \ErrorsDeveloper(  $message . 'The "_where" variable can not have nested values ​​of type "array"' );
			}

			//$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : trim( $value ) );
			$currentValue = is_null( $value ) ? $value : ( is_bool( $value ) ? ( $value ? 1 : 0 ) : $value );
			//Validamos si este campo se debe castear.
			$castVal = $this->getCast( ':', $key, $castDev, $castFw );

			if( $cont > 0 ) {
				$sqlWhere .= '
					AND ';
			}

			//Si la posición es numérica o posición vacía pondremos tal como viene el valor.
			if( is_int($key) || $key === '' ) {
				$sqlWhere .= $value;
				continue;
			}

			switch( $this->cnnData['dbType'] ) {
				case 'SQLServer':
					$sqlWhere .= '[' . $key . ']';
					break;
				case 'PostgreSQL':
					$sqlWhere .= '"' . $key . '"';
					break;
				default:
					$sqlWhere .= $key;
					break;
			}

			//Validamos forma de casteo.
			/*if( $this->cnnData['dbType'] === 'PostgreSQL' || $castVal === '' ) {
				$sqlWhere .= ' = :' . $key . $castVal;
			} else {
				$sqlWhere .= ' = ' . $castVal;
			}*/
			$sqlWhere .= ' = ' . $castVal;
			$sqlValuesArr['bindParams'][ $key ] = $currentValue;
			$cont++;
		}

		$sqlValuesArr['statement'] =
			'DELETE FROM ' . $tableForDelete
				. $sqlWhere;

		if( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) {
			var_dump( $sqlValuesArr );
			die();
		}

		//Ejecutamos el script.
		return $this->execute( $sqlValuesArr, true, true )['rowCount'];
	}

	private function getRememberTime( $pTime ) {
		$rememberTime = 0;

		if( self::$lean->getEnv() === 'production' ) {
			$rememberTime = $pTime;
		}

		return $rememberTime;
	}

	//Método para controlar la transacción.
	private function begin() {
		if( $this->extraConfig['transaction'] === true ) {
			if( self::$cnns[ $this->idCnn ]['oCnn'] && !self::$cnns[ $this->idCnn ]['oCnn']->inTransaction() ) {
				self::$cnns[ $this->idCnn ]['oCnn']->beginTransaction();
			}
		}
	}

	//Método para hacer commit.
	public static function commit( $pIdCnn = null ) {
		try {
			if( !error_get_last() ) {
				$cnns = $pIdCnn ? array( self::$cnns[ $pIdCnn ] ) : self::$cnns;

				foreach( $cnns as $cnn ) {
					if( isset( $cnn['oCnn'] ) && $cnn['oCnn']->inTransaction() ) {
						$cnn['oCnn']->commit();
					}
				}
			}
		} catch( \PDOException $e ) {
			throw new \ErrorsDatabase( 'Error on commit. (' . $e->getMessage() . ')' );
		}
	}

	//Método para hacer rollBack.
	public static function rollBack( $pIdCnn = null ) {
		try {
			$cnns = $pIdCnn ? array( self::$cnns[ $pIdCnn ] ) : self::$cnns;

			foreach( $cnns as $cnn ) {
				if( isset( $cnn['oCnn'] ) && $cnn['oCnn']->inTransaction() ) {
					$cnn['oCnn']->rollBack();
				}
			}
		} catch( \PDOException $e ) {
			throw new \ErrorsDatabase( 'Error on rollback. (' . $e->getMessage() . ')' );
		}
	}

	//Método para cerrar las conexiónes.
	//Nos aseguramos de cerrar las conexiones y que se hagan los commit o rollBack necesarios.
	public static function closeConnections( $pCommit ) {
		try {
			foreach( self::$cnns as $key => &$value ) {
				if( isset( $value['oCnn'] ) && $value['oCnn']->inTransaction() ) {
					if( $pCommit ) {
						$value['oCnn']->commit();
					} else {
						$value['oCnn']->rollBack();
					}
				}

				//Cerramos las conexiones.
				$value['oCnn'] = null;
				$value['stmt'] = null;

				//Inicializamos las propiedades. (Por si el código es ejecutado en memoria)
				self::$instances[ $key ]->initProperties();
			}
		} catch( \PDOException $e ) {
			throw new \ErrorsDatabaseConnection( 'Error closing database connections. (' . $e->getMessage() . ')' );
		} finally {
			self::$instances = null;
			self::$instances = array();
			self::$cnns = null;
			self::$cnns = array();
			self::$cache = array();
		}
	}

	//Método para obtener el CAST que se le debe aplicar a los campos que lo requieren.
	private function getColumnsToCast( $pTable ) {
		$cast = array();
		$rememberTime = $this->getRememberTime(3600);

		//Validate cache.
		if( isset( self::$cache[ $this->idCnn ][ $pTable ]['cast'] ) ) {
			$columns = self::$cache[ $this->idCnn ][ $pTable ]['cast'];
		} else {
			//Leemos la estrucutra de la tabla para hacer los casteos necesarios.
			$columns = $this->execute("
				SELECT column_name, data_type, character_maximum_length
				FROM information_schema.columns
				WHERE table_catalog = '" . $this->cnnData['db'] . "'
				AND table_name = '" . $pTable . "'
			", false, false, false, $rememberTime)['query'];

			//Add to cache.
			self::$cache[ $this->idCnn ][ $pTable ]['cast'] = $columns;
		}

		for( $x=0; $x<count($columns); $x++ ) {
			//Validamos el cast.
			$castResponse = $this->validateCast( $columns[$x]['data_type'], $columns[$x]['column_name'], $columns[$x]['character_maximum_length'] );

			if( $castResponse ) {
				$cast[ $columns[$x]['column_name'] ] = array(
					'type' => $columns[$x]['data_type'],
					'length' => $columns[$x]['character_maximum_length']
				);
			}
		}

		return $cast;
	}

	//Método para obtener la columna casteada.
	private function getCast( $pColumnPrefix, $pColumnName, $pColumnsToCastDev, $pColumnsToCastFw ) {
		$columnsToCast = null;
		$columnsToCastByFw = false;
		$castVal = $pColumnPrefix . $pColumnName;

		if( isset( $pColumnsToCastDev[ $pColumnName ] ) ) {
			$columnsToCast = $pColumnsToCastDev;
		} else if( isset( $pColumnsToCastFw[ $pColumnName ] ) ) {
			$columnsToCast = $pColumnsToCastFw;
			$columnsToCastByFw = true;
		}

		if( $columnsToCast ) {
			if( $columnsToCastByFw ) {
				$dataType = $columnsToCast[ $pColumnName ]['type'];
				$length = $columnsToCast[ $pColumnName ]['length'];
			} else {
				$dataType = $columnsToCast[ $pColumnName ];
				$length = null;
			}

			$dataType = $this->validateCast( $dataType, $pColumnName, $length );

			if( $this->cnnData['dbType'] === 'MySQL' || $this->cnnData['dbType'] === 'SQLServer' ) {
				$castVal = 'CAST( ' . $pColumnPrefix . $pColumnName . ' AS ' . $dataType . ' )';
			} else if( $this->cnnData['dbType'] === 'PostgreSQL' ) {
				$castVal .= '::' . $dataType;
			}
		}

		return $castVal;
	}

	//Método para validar si se debe aplicar el cast
	private function validateCast( $dataType, $columnName, $characterMaximumLength ) {
		$cast = null;

		switch( strtolower( $dataType ) ) {
			case 'boolean':
			case 'bit':
			case 'varchar':
			case 'character varying':
			case 'smallint':
				//MySQL truena al castear tipos de datos string.
				if( $this->cnnData['dbType'] === 'PostgreSQL'
					|| $this->cnnData['dbType'] === 'SQLServer'
					|| !( ( $dataType === 'varchar' || $dataType === 'character varying' ) && $this->cnnData['dbType'] === 'MySQL' )
				) {
					//$cast = $dataType . ( is_null( $characterMaximumLength ) ? '' : '(' . ( $characterMaximumLength < 0 ? 'MAX' : $characterMaximumLength ) . ')' );
					if( $this->cnnData['dbType'] === 'SQLServer' && !is_null( $characterMaximumLength ) ) {
						$cast = $dataType . '(MAX)';
					} else {
						$cast = $dataType;
					}
				}
			default:
				break;
		}

		return $cast;
	}

	//Método para crear el array de "_insert", "_set" y "_where".
	private function createArrayISW( $pTable, $pISW, $pParams ) {
		$pParamsAux = array();
		$prefix = '';

		switch( $pISW ) {
			case '_insert':
				$prefix = '_i-';
				break;
			case '_set':
				$prefix = '_s-';
				break;
			case '_where':
				$prefix = '_w-';
				break;
			default:
				break;
		}

		$messageISW = '(' . $pTable . ') You must specify the values ​​in a "' . $pISW . '" object. Or you can also specify them with the prefix "' . $prefix . '"';

		//Validamos si vienen definidos los parámetros por prefijo.
		if( $pParams && count( $pParams ) > 0 ) {
			foreach( $pParams as $key => $value ) {
				$realKey = explode( $prefix, $key );

				if( $realKey[0] === '' && count( $realKey ) === 2 )
					$pParamsAux[ $realKey[1] ] = $value;
			}

			if( count( $pParamsAux ) === 0 ) {
				throw new \ErrorsDeveloper(  $messageISW );
			}
		} else {
			throw new \ErrorsDeveloper(  $messageISW );
		}

		return $pParamsAux;
	}

	//QManager.

	//Función para crear la consulta en código duro.
	private function qManager( $pParams ) {
		$top = '';
		$query = '';
		$queryAux = '';
		$wao = '';
		$waoField = '';
		$where = false;
		$parenthO = '';
		$parenthC = '';
		$bindParams = array();

		//Armamos el query.
		for( $x=0; $x<count($this->queryArray); $x++ ) {
			$type = trim( $this->queryArray[$x]['type'] );
			$value = is_array( $this->queryArray[$x]['value'] ) ? $this->queryArray[$x]['value'] : trim( $this->queryArray[$x]['value'] );

			switch( $type ) {
				case 'SELECT':
					$query .= $this->getSOWString( $value, 'select' );

					break;
				case 'FROM':
					if( isset( $pParams['_paginate'] ) && $pParams['_paginate'] ) {
						$query .= ' 
						-- & ';
					}

					$fromArr = explode( ' ', $value );
					$fromCount = count($fromArr);
					$from = $fromArr[0];
					$from2 = '';

					switch( $this->cnnData['dbType'] ) {
						case 'SQLServer':
							$from = '[' . $from . ']';
							break;
						case 'PostgreSQL':
							$schema = $this->cnnData['dbType'] === 'PostgreSQL' ? $this->cnnData['schema'] . '.' : '';
							$from = $schema . '"' . $from . '"';
							break;
					}

					for( $y=0; $y<$fromCount; $y++ ) {
						if( $y > 0 ) $from2 .= ' ' . $fromArr[$y];
					}

					$query .= ' 
					FROM ' . $from . $from2;

					break;
				case 'WHERE':
				case 'AND':
				case 'OR':
					$wao = $type;
					$value = $this->getSOWString( $value, 'where' );
					$waoField = $value;

					break;
				case '(':
					$parenthO = '(';

					break;
				case ')':
					$parenthC = ')';
					$query .= ' )';

					if( $x === count($this->queryArray) - 1 ) {
						if( isset( $pParams['_paginate'] ) && $pParams['_paginate'] ) {
							$query .= '
							-- & ';
						}
					}

					break;
				case '=':
				case '!=':
				case '>':
				case '<':
				case '>=':
				case '<=':
				case 'IS NULL':
				case 'IS NOT NULL':
					$bindParamsValue = isset( $pParams[ $value ] ) ? $pParams[ $value ] : null;
					$nullValidation = $type === 'IS NULL' || $type === 'IS NOT NULL';

					//Validamos si debemos cerrar algún paréntesis.
					// if( $parenthC ) {
					// 	$query .= ' )';
					// 	$parenthC = '';
					// }

					if( $wao === 'AND' && !$where ) {
						$wao = 'WHERE';
					}

					if( ( $wao && !is_null($bindParamsValue) ) || $nullValidation ) {
						$query .= ' ' . $wao . ( $parenthO ? ' ' . $parenthO : '' ) . ' ' . $waoField . ' ' . $type . ( $nullValidation ? '' : ' :' . $value );

						if( !$nullValidation ) {
							$bindParams[ $value ] = is_bool( $bindParamsValue ) ? ( $bindParamsValue ? 1 : 0 ) : $bindParamsValue;
						}

						if( $wao === 'WHERE' ) {
							$where = true;
						}
					}

					$wao = '';
					$waoField = '';
					$parenthO = '';

					if( $x === count($this->queryArray) - 1 ) {
						if( isset( $pParams['_paginate'] ) && $pParams['_paginate'] )
							$query .= '
							-- & ';
					}

					break;
				case 'IN':
				case 'NOT IN':
					//Validamos si debemos cerrar algún paréntesis.
					// if( $parenthC ) {
					// 	$query .= ' )';
					// 	$parenthC = '';
					// }

					if( $wao === 'AND' && !$where )
						$wao = 'WHERE';

					if( $wao && isset( $pParams[ $value ] ) ) {
						$query .= ' ' . $wao . ( $parenthO ? ' ' . $parenthO : '' ) . ' ' . $waoField . ' ' . $type . '(' . $pParams[ $value ] . ')';
						//$bindParams[ $value ] = $pParams[ $value ];

						if( $wao === 'WHERE' ) {
							$where = true;
						}
					}

					$wao = '';
					$waoField = '';
					$parenthO = '';

					break;
				case 'LIKE':
				case 'ILIKE':
					//Validamos si debemos cerrar algún paréntesis.
					// if( $parenthC ) {
					// 	$query .= ' )';
					// 	$parenthC = '';
					// }

					if( $wao === 'AND' && !$where ) {
						$wao = 'WHERE';
					}

					//Quitamos los '%' para buscar el campo por el cual hay que filtrar.
					$valueArray = explode( '%', $value );
					$value = '';
					for( $y=0; $y<count($valueArray); $y++ ) {
						//Si es diferente de vacío siginifica que es el campo que andamos buscando.
						if( trim( $valueArray[$y] ) != '' ) {
							$value = $valueArray[$y];
							break;
						}
					}

					if( $wao && isset( $pParams[$value] ) ) {
						$queryAux = '';

						//Concatenamos el valor junto con los '%'.
						for( $y=0; $y<count($valueArray); $y++ ) {
							if( trim( $valueArray[$y] ) === '' ) {
								$queryAux .= '%';
							} else {
								$queryAux .= $pParams[$value];
							}
						}

						$query .= ' ' . $wao . ( $parenthO ? ' ' . $parenthO : '' ) . ' ' . $waoField . ' ' . $type .  " '" . $queryAux . "' ";

						if( $wao === 'WHERE' ) {
							$where = true;
						}
					}

					$wao = '';
					$waoField = '';
					$parenthO = '';

					if( $x === count($this->queryArray) - 1 ) {
						if( isset( $pParams['_paginate'] ) && $pParams['_paginate'] ) {
							$query .= '
							-- & ';
						}
					}

					break;
				case 'BETWEEN':
					//Validamos si debemos cerrar algún paréntesis.
					// if( $parenthC ) {
					// 	$query .= ' )';
					// 	$parenthC = '';
					// }

					if( $wao === 'AND' && !$where ) {
						$wao = 'WHERE';
					}

					if( $wao && isset( $pParams[ $value[0] ] ) && isset( $pParams[ $value[1] ] ) ) {
						$query .= ' ' . $wao . ( $parenthO ? ' ' . $parenthO : '' ) . ' '
							. $waoField . ' ' . $type . ' '
							. ':' . $value[0] . ' AND '
							. ':' . $value[1];
						$bindParams[ $value[0] ] = $pParams[ $value[0] ];
						$bindParams[ $value[1] ] = $pParams[ $value[1] ];

						if( $wao === 'WHERE' ) {
							$where = true;
						}
					}

					$wao = '';
					$waoField = '';
					$parenthO = '';

					if( $x === count($this->queryArray) - 1 ) {
						if( isset( $pParams['_paginate'] ) && $pParams['_paginate'] ) {
							$query .= '
							-- & ';
						}
					}

					break;
				case 'GROUP BY':
					$value = $this->getSOWString( $value, 'groupby' );
				case 'LIMIT':

					//Validamos si debemos cerrar algún paréntesis.
					// if( $parenthC ) {
					// 	$query .= ' )';
					// 	$parenthC = '';
					// }

					if( $type === 'LIMIT ' && $this->cnnData['dbType'] === 'SQLServer' ) {
						$top = 'TOP ' . $value . ' ';
					} else {
						$query .= ' ' . $type . ' ' . $value;
					}

					if( $x === count($this->queryArray) - 1 ) {
						if( isset( $pParams['_paginate'] ) && $pParams['_paginate'] ) {
							$query .= ' 
							-- & ';
						}
					}

					break;

				case 'ORDER BY':
					//Validamos si debemos cerrar algún paréntesis.
					// if( $parenthC ) {
					// 	$query .= ' )';
					// 	$parenthC = '';
					// }

					$query .= ' ' . $type . ' ' . $value;

					break;
				default:
					break;
			}
		}

		//Valida if the "query" is correct.
		if( !$query ) {
			throw new \ErrorsDeveloper( 'Be sure to set the correct parameters for the "exec" method' );
		}

		if( ( isset( $pParams['_showExec'] ) && $pParams['_showExec'] === true ) && ( isset( $pParams['_paginate'] ) && !$pParams['_paginate'] ) ) {
			var_dump( $query );
			die();
		}

		$this->queryArray = array();
		$queryReturn = ( isset( $pParams['_paginate'] ) && $pParams['_paginate'] ? '
			-- & ' : '' ) . '
			SELECT ' . $top . $query;
		$queryReturn = implode("\n", array_map('trim', explode("\n", $queryReturn)));

		return [
			'statement' => $queryReturn,
			'bindParams' => $bindParams
		];
	}

	public function select( $pSelect = '*' ) {
		array_push($this->queryArray, array(
			'type' => 'SELECT',
			'value' => ( is_array( $pSelect ) ? join( ', ', $pSelect ) : $pSelect )
		));

		return $this;
	}

	public function from( $pTable ) {
		if( !$pTable ) {
			throw new \ErrorsDeveloper( 'You must specify the name of the table' );
		}

		array_push($this->queryArray, array(
			'type' => 'FROM',
			'value' => $pTable
		));

		return $this;
	}

	public function where( $pField ) {
		array_push($this->queryArray, array(
			'type' => 'WHERE',
			'value' => $pField
		));

		return $this;
	}

	public function _and( $pField ) {
		array_push($this->queryArray, array(
			'type' => 'AND',
			'value' => $pField
		));

		return $this;
	}

	public function andWhere( $pField ) {
		array_push($this->queryArray, array(
			'type' => 'AND',
			'value' => $pField
		));

		return $this;
	}

	public function _or( $pField ) {
		array_push($this->queryArray, array(
			'type' => 'OR',
			'value' => $pField
		));

		return $this;
	}

	public function orWhere( $pField ) {
		array_push($this->queryArray, array(
			'type' => 'OR',
			'value' => $pField
		));

		return $this;
	}

	public function groupBy( $pField ) {
		if( $pField ) {
			array_push($this->queryArray, array(
				'type' => 'GROUP BY',
				'value' => $pField
			));
		}

		return $this;
	}

	public function orderBy( $pField ) {
		if( $pField ) {
			array_push($this->queryArray, array(
				'type' => 'ORDER BY',
				'value' => $pField
			));
		}

		return $this;
	}

	public function limit( $pLimit ) {
		if( $pLimit ) {
			array_push($this->queryArray, array(
				'type' => 'LIMIT',
				'value' => $pLimit
			));
		}

		return $this;
	}

	public function parenth( $pParenth ) {
		array_push($this->queryArray, array(
			'type' => $pParenth,
			'value' => $pParenth
		));

		return $this;
	}

	/*** FUNCIONES DE COMPARACIÓN ***/

	public function eq( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => '=',
			'value' => $pValue
		));

		return $this;
	}

	public function neq( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => '!=',
			'value' => $pValue
		));

		return $this;
	}

	public function in( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => 'IN',
			'value' => $pValue
		));

		return $this;
	}

	public function notin( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => 'NOT IN',
			'value' => $pValue
		));

		return $this;
	}

	public function like( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => 'LIKE',
			'value' => $pValue
		));

		return $this;
	}

	public function ilike( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => 'ILIKE',
			'value' => $pValue
		));

		return $this;
	}

	public function between( $pValue1 = null, $pValue2 = null ) {
		array_push($this->queryArray, array(
			'type' => 'BETWEEN',
			'value' => array( $pValue1, $pValue2 )
		));

		return $this;
	}

	public function gt( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => '>',
			'value' => $pValue
		));

		return $this;
	}

	public function lt( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => '<',
			'value' => $pValue
		));

		return $this;
	}

	public function gte( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => '>=',
			'value' => $pValue
		));

		return $this;
	}

	public function lte( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => '<=',
			'value' => $pValue
		));

		return $this;
	}

	public function isNull( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => 'IS NULL',
			'value' => $pValue
		));

		return $this;
	}

	public function isNotNull( $pValue = null ) {
		array_push($this->queryArray, array(
			'type' => 'IS NOT NULL',
			'value' => $pValue
		));

		return $this;
	}

	public function lock( $pTable, $pType ) {
		$lock = null;
		$type = null;

		switch( $this->cnnData['dbType'] ) {
			case 'PostgreSQL':
				$type = $pType ? $pType : 'ACCESS EXCLUSIVE';
				$typeRepl = str_replace( ' ', '_', $type );
				$lock = 'LOCK TABLE ' . $this->getSOWString( $pTable, 'table' ) . ' IN ' . $type . ' MODE;';
				break;
		}

		//Nos aseguramos que no se repita un lock ya establecido.
		if( $lock && !( isset( $this->locks[ $typeRepl ] ) && $this->locks[ $typeRepl ] ) ) {
			$this->execute( $lock, false, true, false, 0 )['query'];
			array_push($this->locks[ $this->cnnData['dbType'] ], array(
				$type => true
			));
		}
	}
}