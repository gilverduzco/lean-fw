<?php
class BaseModel {
    protected $connection;
    protected $table;
    protected $fields;

    public function __construct( $pIdCnn = null, $pExtraConfig = null ) {
        $this->connection = \DBAccess::getInstance($pIdCnn, $pExtraConfig);
    }

    private function getTable() {
        return explode( ':', $this->table )[0];
    }

    private function getShowExec( $pData ) {
        return isset( $pData['_showExec'] ) ? $pData['_showExec'] : null;
    }

    private function validateClauses( $pSet, $pWhere ) {
        if( !is_null($pSet) ) {
            if( !is_array( $pSet ) ) {
                throw new \ErrorsDeveloper('You must specify an array for the "set" clause.');
            }
        }

        if( !is_array( $pWhere ) ) {
            throw new \ErrorsDeveloper('You must specify an array for the "where" clause.');
        }
    }

    public function get( $pData = null, $pGetOne = false ) {
        $pData = is_array($pData) ? $pData : [];
        $oDBA = $this->connection
            ->select( $this->fields ? $this->fields : '*' )
            ->from( $this->getTable() );
        
        if( is_array( $pData ) ) {
            $params = array();
            $paginate = isset( $pData['_paginate'] ) && $pData['_paginate'];
            $where = false;
            $result = null;

            foreach( $pData as $key => $value ) {
                $key = trim( $key );
                $params[ $key ] = $value;

                if( !in_array( $key, ['_paginate', '_page', '_rows', '_slt', '_orderBy', '_showExec', '_cast'] ) ) {
                    if( !$where ) {
                        $where = true;
                        $oDBA->where( $key );
                    } else {
                        $oDBA->andWhere( $key );
                    }

                    $oDBA->eq( $key );
                } else {
                    unset( $key );
                }
            }

            if( $paginate && !isset($pData['_slt']) ) {
                $params['_slt'] = $this->fields;
            }

            if( $pGetOne ) {
                $result = $oDBA->getOne( $params );
            } else {
                $result = $oDBA->exec( $params );
            }

            return $result;
        }

        return $oDBA;
    }

    public function getOne( $pData = null ) {
        return $this->get( $pData, true );
    }

    public function insert( $pData ) {
        if( !is_array( $pData ) ) {
            throw new \ErrorsDeveloper('You must specify an array for the "insert" clause.');
        }

        $showExec = $this->getShowExec($pData);
        unset( $pData['_showExec'] );

        return $this->connection->insert( $this->table, ['_insert' => $pData, '_showExec' => $showExec] );
    }

    public function update( $pSet, $pWhere ) {
        $this->validateClauses( $pSet, $pWhere );
        $showExec = $this->getShowExec(array_merge( $pSet, $pWhere ));
        unset( $pSet['_showExec'] );
        unset( $pWhere['_showExec'] );

        $this->connection->update( $this->getTable(), ['_set' => $pSet, '_where' => $pWhere, '_showExec' => $showExec] );
    }

    public function updateOne( $pSet, $pWhere ) {
        $this->validateClauses( $pSet, $pWhere );
        $showExec = $this->getShowExec(array_merge( $pSet, $pWhere ));
        unset( $pSet['_showExec'] );
        unset( $pWhere['_showExec'] );

        $this->connection->updateOne( $this->getTable(), ['_set' => $pSet, '_where' => $pWhere, '_showExec' => $showExec] );
    }

    public function delete( $pWhere ) {
        $this->validateClauses( null, $pWhere );
        $showExec = $this->getShowExec($pWhere);
        unset( $pWhere['_showExec'] );
        
        $this->connection->delete( $this->getTable(), ['_where' => $pWhere, '_showExec' => $showExec] );
    }

    public function deleteOne( $pWhere ) {
        $this->validateClauses( null, $pWhere );
        $showExec = $this->getShowExec($pWhere);
        unset( $pWhere['_showExec'] );
        
        $this->connection->deleteOne( $this->getTable(), ['_where' => $pWhere, '_showExec' => $showExec] );
    }

    public function lock( $pType = null ) {
        return $this->connection->lock( $this->getTable(), $pType );
    }
}