<?php
class Helper {
    public static function getArgv() {
        $argv = $_SERVER['argv'];
        $argvCount = count($argv);
        $params = [];
        
        for( $x=1; $x<$argvCount; $x++ ) {
            $param = explode( '--', $argv[$x] );
            $guion = substr($argv[$x], 0, 2);
    
            if( $guion === '--' && count( $param ) == 2 ) {
                $param = explode( '=', $param[1] );
                $params[ $param[0] ] = $param[1] ?? null;
            }
        }

        return $params;
    }

    public static function isOk($pThrow = true) {
        $errorGetLast = error_get_last();

        if( $errorGetLast && $pThrow === true ) {
            throw new Exception( $errorGetLast['message'] );
        } else {
            return $errorGetLast ? false : true;
        }
    }

    public static function getEnv() {
        return \Lean::getInstance()->getEnv();
    }

    public static function getDebug() {
        return \Lean::getInstance()->getDebug();
    }

    public static function getSiteName() {
        $siteName = self::getConfig('siteName');

        if( is_null($siteName) ) {
            $siteName = 'site-without-name';
        }
        
        return $siteName;
    }

    public static function getIsCli() {
        return \Lean::getInstance()->getIsCli();
    }

    public static function translate( $pMessage, $pExtraValues ) {
        $lean = \Lean::getInstance();
        $language =  $lean->getLanguage();
        $messageT = $pMessage;
        
        if( $language ) {
            // Check if the message needs to be translated
            if( preg_match( '/^{/', $messageT ) && preg_match( '/}$/', $messageT ) ) {
                $strings = $lean->getJSONFile( 'strings', $language . '.string' );
                
                if( $strings ) {
                    $key = str_replace('{', '', $messageT);
                    $key = str_replace('}', '', $key);
                    $value = isset( $strings[$key] ) && $strings[$key] ? $strings[$key] : null;

                    if( $value ) {
                        $messageT = str_replace( '{' . $key . '}', $value, $messageT );
                    } else {
                        throw new \ErrorsDeveloper('Constant "' . $key . '" not found in strings file.');
                    }

                    if( $pExtraValues ) {
                        foreach( $pExtraValues as $key => $value ) {
                            $messageT = str_replace( '{' . $key . '}', $value, $messageT );
                        }
                    }
                }
            }
        }

        return $messageT;
    }

    public static function getHeaders( $pKey = null ) {
        $router = \Lean::getInstance()->getRouter();

        if( $router ) {
            return $router->getRequest()->getHeaders( $pKey );
        }

        return null;
    }

    public static function getConfig( $pKey = null ) {
        return \Lean::getInstance()->getConfig( $pKey );
    }

    public static function getController( $pRoute ) {
        return \Lean::getInstance()->getController($pRoute);
    }

    public static function getDomain( $pRoute, $pDomain = null ) {
        $pRoute = is_null($pDomain) ? $pRoute : $pRoute . '/' . $pDomain; 

        return \Lean::getInstance()->getDomain($pRoute);
    }

    public static function getModel( $pRoute, $pIdCnn = null, $pExtraConfig = null ) {
        return \Lean::getInstance()->getModel($pRoute, $pIdCnn, $pExtraConfig);
    }

    private static function validateConstraintParams( $pConstraint, $pMessage, $pMethod ) {
        if( is_null( $pConstraint ) || is_null( $pMessage ) ) {
            throw new \ErrorsDeveloper( 'Be sure to specify the correct parameters for the "' . $pMethod . '" method.' );
        }
    }

    public static function customPrimaryKey( $pPK = null, $pMessage = null, $pExtraValues = null ) {
        self::validateConstraintParams( $pPK, $pMessage, __FUNCTION__ );
        \Lean::getInstance()->setCustomMessage( 'pk', $pPK, self::translate( trim( $pMessage ), $pExtraValues ) );
    } 

    public static function customForeignKey( $pFK = null, $pMessage = null, $pExtraValues = null ) {
        self::validateConstraintParams( $pFK, $pMessage, __FUNCTION__ );
        \Lean::getInstance()->setCustomMessage( 'fk', $pFK, self::translate( trim( $pMessage ), $pExtraValues ) );
    } 

    public static function customUniqueKey( $pUniqueC = null, $pMessage = null, $pExtraValues = null ) {
        self::validateConstraintParams( $pUniqueC, $pMessage, __FUNCTION__ );
        \Lean::getInstance()->setCustomMessage( 'uq', $pUniqueC, self::translate( trim( $pMessage ), $pExtraValues ) );
    } 
}