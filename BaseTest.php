<?php
use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase {
    private $lean;

    public function __construct() {
        parent::__construct();
        $this->lean = \Lean::getInstance();
    }

    public function getConfig( $pKey = null ) {
        return $this->lean->getConfig( $pKey );
    }

    public function getEnv() {
        return $this->lean->getEnv();
    }    

    public function getModule() {
        return $this->lean->getRouter()->getModule();
    }
}