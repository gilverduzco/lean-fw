<?php
class Request {
    private $baseURI;
    private $uri;
    private $verb;
    private $headers;
    private $body;
    private $bodyString;
    private $query;
    private $params;
    private $token;
    private $tokenData;

    public function __construct() {
        if( Helper::getIsCli() === false ) {
            $siteName = \Helper::getSiteName();
            $this->uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
            // $this->uri = $siteName ? str_replace( $siteName . '/' , '', $this->uri ) : $this->uri;
            $this->uri = $siteName ? preg_replace('/' . preg_quote($siteName . '/', '/') . '/', '', $this->uri, 1) : $this->uri;
            $this->uri = strtok( $this->uri, '?' ); // Quitamos el query string params.
            $this->verb = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
            parse_str( isset( $_SERVER['QUERY_STRING'] ) ? $_SERVER['QUERY_STRING'] : null, $this->query );
            $this->query = $this->query ? $this->query : null;
            $this->bodyString = file_get_contents('php://input');
            $this->bodyString = $this->bodyString ? $this->bodyString : null;
            $this->body = json_decode( $this->bodyString, true );
            $this->setHeaders();
        }
    }

    private function setHeaders() {
        $headers = getallheaders();

        foreach( $headers as $key => $value ) {
            $this->headers[ strtolower($key) ] = $value;
        }
    }

    private function getCast( $pKey ) {
        $key = $pKey;
        $castTo = null;

        if( $pKey ) {
            $cast = explode( '|', $pKey );
            $countX = count($cast);
            
            if( $countX > 1 ) {
                $key = $cast[0];
                $castTo = $cast[1];
            }
        }

        return [
            'key' => $key,
            'castTo' => $castTo
        ];
    }

    private function cast( $pValue, $pCastTo ) {
        $value = $pValue;
        $value = is_string($value) ? trim($value) : $value;

        if( !is_null($pCastTo) && !is_null($value) ) {
            switch( $pCastTo ) {
                case 'int':
                case 'integer':
                case 'numeric':
                case 'number':
                    $value = (int)$value;
                    break;
                case 'float':
                case 'double':
                    $value = (float)$value;
                    break;
                case 'bool':
                case 'boolean':
                    $boolsArrTrue = ['true', '1'];
                    $boolsArrFalse = ['false', '0'];
                    $value = in_array( $value, $boolsArrTrue, true ) ? true : ( in_array( $value, $boolsArrFalse, true ) ? false : (bool)$value );
                    break;
                case 'string':
                    $value = (string)$value;
                    break;
            }
        }
        
        return $value;
    }

    public function setBaseURI( $pBaseURI )  {
        $this->baseURI = $pBaseURI;
    }

    public function setAllParams( $pParams ) {
        $this->headers = isset( $pParams['headers'] ) ? $pParams['headers'] : null;
        $this->tokenData = isset( $pParams['tokenData'] ) ? $pParams['tokenData'] : null;
        $this->query = isset( $pParams['query'] ) ? $pParams['query'] : null;
        $this->bodyString = isset( $pParams['bodyString'] ) ? $pParams['bodyString'] : null;
        $this->body = isset( $pParams['body'] ) ? $pParams['body'] : null;
        $this->params = isset( $pParams['params'] ) ? $pParams['params'] : null;
    }

    public function setParams( $pValue ) {
        $this->params = $pValue;
    }

    public function setBody( $pKey, $pValue ) {
        $this->body[$pKey] = $pValue;
    }

    public function setToken( $pToken ) {
        $this->token = $pToken;
    }

    public function setTokenData( $pValue ) {
        $this->tokenData = $pValue;
    }

    public function setHeader( $pKey, $pValue ) {
        $this->headers[$pKey] = $pValue;
    }

    public function getUri() {
        return $this->uri;
    }

    public function getBaseURI() {
        return $this->baseURI;
    }

    public function getVerb() {
        return $this->verb;
    }

    public function getHeaders( $pKey = null ) {
        $keyCast = $this->getCast($pKey);

        return $this->cast((
            $pKey 
                ? ( 
                    isset( $this->headers[ $keyCast['key'] ] ) 
                        ? $this->headers[ $keyCast['key'] ]
                        : (
                            isset( $this->headers[ strtolower( $keyCast['key'] ) ] )
                                ? $this->headers[ strtolower( $keyCast['key'] ) ]
                                : null
                        )
                )
                : $this->headers
        ), $keyCast['castTo']);
    }

    public function getBodyString() {
        return $this->bodyString;
    }

    public function getBody( $pKey = null ) {
        $keyCast = $this->getCast($pKey);

        return $this->cast( ( $pKey ? ( isset( $this->body[ $keyCast['key'] ] ) ? $this->body[ $keyCast['key'] ] : null ) : $this->body ), $keyCast['castTo'] );
    }

    public function getQuery( $pKey = null ) {
        $keyCast = $this->getCast($pKey);

        return $this->cast( ( $pKey ? ( isset( $this->query[ $keyCast['key'] ] ) ? $this->query[ $keyCast['key'] ] : null ) : $this->query ), $keyCast['castTo'] );
    }

    public function getParams( $pKey = null ) {
        $keyCast = $this->getCast($pKey);

        return $this->cast( ( $pKey ? ( isset( $this->params[ $keyCast['key'] ] ) ? $this->params[ $keyCast['key'] ] : null ) : $this->params ), $keyCast['castTo'] );
    }

    public function getAllParams() {
        $params = $this->body ? $this->body : array();
        $params = array_merge( $params, $this->query ? $this->query : array() );
        $params = array_merge( $params, $this->params ? $this->params : array() );

        return $params;
    }

    public function getToken() {
        return $this->token;
    }

    public function getTokenData( $pKey = null ) {
        $keyCast = $this->getCast($pKey);

        return $this->cast( ( $pKey ? ( isset( $this->tokenData[ $keyCast['key'] ] ) ? $this->tokenData[ $keyCast['key'] ] : null ) : $this->tokenData ), $keyCast['castTo'] );
    }
}