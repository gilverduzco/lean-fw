<?php
class BaseDomain {
    private $lean;
    protected $modelName;
    protected $model;

    public function __construct() {
        $this->lean = \Lean::getInstance();
    }

    public function getConfig( $pKey = null ) {
        return \Helper::getConfig($pKey);
    }

    public function setError( $pMessage, $pExtraValues = null ) {
        throw new \ErrorsDomain( $pMessage, $pExtraValues );
    }

    public function getDomain( $pRoute, $pDomain = null ) {
        return \Helper::getDomain($pRoute, $pDomain);
    }

    public function getModel( $pRoute, $pIdCnn = null, $pExtraConfig = null ) {
        return \Helper::getModel($pRoute, $pIdCnn, $pExtraConfig);
    }

    public function getEnv() {
        return $this->lean->getEnv();
    }

    public function getModule() {
        return $this->lean->getRouter()->getModule();
    }

    public function onFinish( $pFnc ) {
        $this->lean->onFinish( $pFnc );
    }
}