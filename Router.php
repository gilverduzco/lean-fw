<?php
class Router {
	//Propiedades de la clase.
	private $lean;
	private $module;
	private $request;
	private $routesFound;
	private $routeFound;
	private $controller;
	private $method;
	private $methodBefore;
	private $verifyToken;
	private $routesToResponse;
	private $options;
	private $showDetailErrors;	
	private $middlewareGlobal;	
	private $middlewareByVerb;	
	private $middlewareByUri;
	private $middlewareGlobalAfter;	
	private $middlewareByVerbAfter;	
	private $middlewareByUriAfter;	
	private $middlewareResponse;

	public function __construct() {
		$this->lean = \Lean::getInstance();
		$this->request = new \Request();
		$this->module = null;
		$this->routesFound = [];
		$this->routeFound = null;
		$this->controller = null;
		$this->method = null;
		$this->methodBefore = null;
		$this->verifyToken = null;
		$this->routesToResponse = null;
		$this->options = false;
		$this->showDetailErrors = false;
		$this->middlewareGlobal = null;
		$this->middlewareByVerb = null;
		$this->middlewareByUri = null;
		$this->middlewareGlobalAfter = null;
		$this->middlewareByVerbAfter = null;
		$this->middlewareByUriAfter = null;
		$this->middlewareResponse = [];
	}

	public function getModule() {
		return $this->module;
	}

	public function getRequest() {
		return $this->request;
	}

	private function setMiddlewareResponse( $pType, $pValue ) {
		$this->middlewareResponse[$pType] = $pValue;
	}

	public function getMiddlewareResponse( $pKey = null ) {
		return $pKey ? ( isset( $this->middlewareResponse[$pKey] ) ? $this->middlewareResponse[$pKey] : null ) : $this->middlewareResponse;
	}

	private function setCors( $pCors ) {
		$httpOrigin = null;
		$originAllowed = '*';
		$headersAllowed = '*';
		$methodsAllowed = '*';
		$credentialsAllowed = 'true';
		
		if( !is_null($pCors) ) {
			$originAllowed = isset( $pCors['origin'] ) ? $pCors['origin'] : '*';
			$headersAllowed = isset( $pCors['headers'] ) ? $pCors['headers'] : '*';
			$methodsAllowed = isset( $pCors['methods'] ) ? $pCors['methods'] : '*';
			$credentialsAllowed = isset( $pCors['credentials'] ) ? $pCors['credentials'] : 'true';
		}

		if( isset( $_SERVER['HTTP_ORIGIN'] ) ) {
			$httpOrigin = $_SERVER['HTTP_ORIGIN'];
		} else if( isset( $_SERVER['HTTP_REFERER'] ) ) {
			$httpOrigin = $_SERVER['HTTP_REFERER'];
		} else if( isset( $_SERVER['REMOTE_ADDR'] ) ) {
			$httpOrigin = $_SERVER['REMOTE_ADDR'];
		}

		if( !is_null($httpOrigin) ) {
			$httpOriginAllowed = is_array( $originAllowed ) ? $originAllowed : [$originAllowed];
			$httpOriginAllowedCount = sizeof( $httpOriginAllowed );

			for( $x=0; $x<$httpOriginAllowedCount; $x++ ) {
				if( 
					$httpOriginAllowed[$x] === '*' || 
					$httpOriginAllowed[$x] === $httpOrigin || 
					($httpOriginAllowed[$x] . '/') === $httpOrigin ||
					$httpOriginAllowed[$x] === ($httpOrigin . '/') ||
					($httpOriginAllowed[$x] . '/') === ($httpOrigin . '/')
				) {
					header('Access-Control-Allow-Origin: ' . $httpOriginAllowed[$x] );
					break;
				}
			}
			
			header('Access-Control-Allow-Credentials: ' . $credentialsAllowed );
			header('Access-Control-Allow-Headers: ' . $headersAllowed );
			header('Access-Control-Allow-Methods: ' . $methodsAllowed );
			header('Access-Control-Max-Age: 3600' );
		}
	}

	public function init() { 
		$this->routesFound = [];
		$this->routeFound = false;
		$debug = $this->lean->getConfig('debug');
		$this->showDetailErrors = $debug ? $debug : false;
		$url = $this->request->getUri();
		$moduleArr = explode( '/', $url );
		$this->module = count( $moduleArr ) > 0 ? $moduleArr[1] : null;
		$requestVerb = strtolower( $this->request->getVerb() );

		if( !$this->module ) {
			die('Hi!');
		} else {
			$routesFile = _MODULES . '/' . $this->module . '/routes.config.php';
		}
		
		//Validamos si es un options y buscamos la ruta a la cual quiere ir.
		if( $requestVerb === 'options' )  {
			$requestVerb = $this->request->getHeaders('Access-Control-Request-Method');
			$this->options = true;
			return;
		}

		if( count( glob($routesFile) ) ) {
			$routes = require $routesFile;
			$handler = null;
			$url = strtok( str_replace( $this->module . '/', '', $url ), '?' ); // Quitar nombre del módulo de URI
			$routesVerb = isset( $routes[ $requestVerb ] ) && is_array( $routes[ $requestVerb ] ) && $routes[ $requestVerb ] ? $routes[ $requestVerb ] : array();
			$this->middlewareGlobal = isset( $routes['middleware'] ) && $routes['middleware'] ? $routes['middleware'] : null;
			$this->middlewareByVerb = isset( $routesVerb['middleware'] ) && $routesVerb['middleware'] ? $routesVerb['middleware'] : null;
			$this->middlewareGlobalAfter = isset( $routes['middleware:after'] ) && $routes['middleware:after'] ? $routes['middleware:after'] : null;
			$this->middlewareByVerbAfter = isset( $routesVerb['middleware:after'] ) && $routesVerb['middleware:after'] ? $routesVerb['middleware:after'] : null;
			
			//Se recorren las rutas válidas.
			foreach( $routesVerb as $key => $route ) {
				//if( $this->routeFound ) break;

				if( $key === 'middleware' || $key === 'middleware:after' ) continue;
					
				if( !isset( $route['uri'] ) ) {
					throw new \ErrorsDeveloper( 'Be sure to define the "uri" property in all of your routes.'
						. ( $this->showDetailErrors ? ' (' . $routesFile . ')' : '' ) );
				}

				if( !isset( $route['handler'] ) ) {
					throw new \ErrorsDeveloper( 'Be sure to define the "handler" property in all of your routes.'
						. ( $this->showDetailErrors ? ' (' . $routesFile . ')' : '' ) );
				}

				if( !isset( $route['method'] ) ) {
					throw new \ErrorsDeveloper( 'Be sure to define the "method" property in all of your routes.'
						. ( $this->showDetailErrors ? ' (' . $routesFile . ')' : '' ) );
				}

				$uri = $route['uri'];
				$handler = $route['handler'];
				$urlArr = explode( '/', $url );
				$urlArrCount = count( $urlArr );
				$uriArr = explode( '/', $uri );
				$uriArrCount = count( $uriArr );
				$reEx = '';
				
				//Validación de la URI.
				if( $urlArrCount === $uriArrCount ) {
					for( $y = 0; $y < $uriArrCount; $y++ ) {
						if( $uriArr[$y] !== '' ) {
							//if( $reEx === '' ) {
								//$reEx .= '/' . $uriArr[$y];
								
								//Las uri's no deben empezar con un valor dinámico. Ejemplo: /{idGil}/ejemplo
								// if( preg_match( '/^{/', $uriArr[$y] ) && preg_match( '/}$/', $uriArr[$y] ) ) {
								// 	throw new \ErrorsDeveloper('This resource (' . $uri . ') is invalid. The uri can not start with "/{key}".');
								// }
							//} else {
								if( preg_match( '/^{/', $uriArr[$y] ) && preg_match( '/}$/', $uriArr[$y] ) ) {
									$reEx .= '/(.+)';
								
									//Con esto validremos que no vengan dos variables juntas en la URI.
									//Esto significa que la URI está mal declarada.
									/*if( $lastY === $y - 1 ) {
										if( explode( '/', $url )[1] === explode( '/', $uri )[1] ) {
											$uriWrong = true;
											$uriInterfere = $uri;
											break;
										}
									} else {
										$lastY = $y;
									}*/
								} else {
									$reEx .= '/' . $uriArr[$y];
								}
							//}
						}
					}

					$reEx = '~^' . $reEx . '$~';

					if( preg_match( $reEx, $url ) ) {
						$uriParams = array();

						//Obtenemos los parámetros.
						for( $y=0; $y<count( $uriArr ); $y++ ) {	
							if( preg_match( '/^{/', $uriArr[$y] ) && preg_match( '/}$/', $uriArr[$y] ) )	
								$uriParams[ str_replace( '}', '', str_replace( '{', '', $uriArr[$y] ) ) ] = $urlArr[$y];
						}
						
						$this->routeFound = true;
						array_push($this->routesFound, [
							'url' => $url,
							'uri' => $uri,
							'cors' => isset( $route['cors'] ) && $route['cors'],
							'controller' => $handler,
							'method' => $route['method'],
							'methodBefore' => isset( $route['before'] ) ? $route['before'] : null,
							'verifyToken' => isset( $route['verifyToken'] ) ? $route['verifyToken'] : true,
							'requestParams' => $uriParams ? $uriParams : null,
							'requestBaseURI' => '/' . $this->module . $uri,
							'middlewareByUri' => isset( $route['middleware'] ) && $route['middleware'] ? $route['middleware'] : null,
							'middlewareByUriAfter' => isset( $route['middleware:after'] ) && $route['middleware:after'] ? $route['middleware:after'] : null
						]);
					}
				}
			}

			if( !$this->routeFound ) {
				/*if( $uriWrong ) {
					$e = new \ErrorsRouter('The uri "' . $uriInterfere . '" is interfering with the uri "' . $url . '". Please, solve this problem.');
					$e::withHttpStatusCode(500);
					throw $e;
				} else {*/
					if( '/' . $this->module === $url && $this->lean->getEnv() !== 'production' ) {
						$this->routesToResponse = $routes;
						return;
					}
					
					$e = new \ErrorsRouter('Resource "' . $url . '" not found in module "' . $this->module . '".');
					$e::withHttpStatusCode(404);
					throw $e;
				//}
			}
		} else {
			$e = new \ErrorsRouter('There is not a routes file for module "' . $this->module . '" .');
			$e::withHttpStatusCode(400);
			throw $e;
		}
		
		return $this;
	}

	public function getVerifyToken() {
		return $this->verifyToken;
	}

	private function getToken( $pTokenRequired, $pSecretString ) {
		$tokenVerify = 'invalid';
		$authorization = $this->request->getHeaders('Authorization');

		if( $this->verifyToken ) {
			if( $authorization ) {
				$token = str_replace('Bearer ', '', $authorization);
				$verifyTokenCustom = $this->lean->getConfig('verifyToken');
				$this->request->setToken($token);

				//Verificamos si se sobreescribió el método de validación del token.
				if( is_string($verifyTokenCustom) ) {
					$verifyTokenCustomArr = explode( '@', $verifyTokenCustom );
					$verifyTokenCustomClass = null;
					$verifyTokenCustomMethod = null;

					if( count( $verifyTokenCustomArr ) === 2 ) {
						$verifyTokenCustomClass = $verifyTokenCustomArr[0];
						$verifyTokenCustomMethod =  $verifyTokenCustomArr[1];
						$className = '';
						$classNameArray = explode('-', $verifyTokenCustomClass);

						foreach( $classNameArray as $elem ) {
							$className .= ucfirst($elem);
						}
			
						$className .= 'Middleware';
						$middleWareFile = _APP . '/middleware/' . $verifyTokenCustomClass . '.middleware.php';

						if( count( glob( $middleWareFile ) ) ) {
							require_once $middleWareFile;

							$oMiddleWare = new $className();
							$this->setMiddlewareResponse( 'verifyTokenCustom', $oMiddleWare->$verifyTokenCustomMethod( $this->request ) );
						} else {
							throw new \ErrorsDeveloper('There is not a middleware for override method "verifyToken".');
						}
					} else {
						throw new \ErrorsDeveloper('Please, check that the middleware name has the correct nomenclature (' . $verifyTokenCustom . ').');
					}
				} else if( $pTokenRequired ) {
					\Auth::verify( $token, $pSecretString );
				}

				$tokenVerify = 'valid';
			} else if( $pTokenRequired ) {
				throw new \ErrorsAuth('Authorization token is required.');
			} else $tokenVerify = 'valid';
		} else {
			$tokenVerify = 'valid';
		}

		if( $tokenVerify === 'invalid' ) {
			throw new \ErrorsAuth('Invalid session.');
		}
	}

	private function getFileClassName( $pController ) {
		//Obtenemos el controlador y ejecutamos el método correspondiente.
		$controller = explode( ':', $pController );

		//Validamos que el controller tenga la nomenclatura correcta.
		if( !( count( $controller ) === 1 || count( $controller ) === 2 ) ) {
			throw new \ErrorsDeveloper( 'Please, check that the controller name has the correct nomenclature.'
				. ( $this->showDetailErrors ? ' (' . $pController . ')' : '' ) );
		}

		$fileName = strtolower( $controller[0] ) . '.controller';
		$class = ( count( $controller ) === 2 ? $controller[1] : $controller[0] );
		$className = '';
		$classNameArray = explode('-', $class);

		foreach( $classNameArray as $elem ) {
			$className .= ucfirst($elem);
		}

		$className .= 'Controller';

		require_once _MODULES . '/' . $this->module . '/controllers/' . $fileName . '.php';

		return array(
			'fileName' => $fileName,
			'className' => $className
		);
	}

	private function middleware( $pAfter, $pRouteMiddleware, $pResponse, $pType ) {
		$response = null;
		$middlewareArr = explode( '@', $pRouteMiddleware );
		$middlewareClass = null;
		$middlewareMethod = null;

		if( count( $middlewareArr ) === 2 ) {
			$middlewareClass = $middlewareArr[0];
			$middlewareMethod =  $middlewareArr[1];
			$className = '';
			$classNameArray = explode('-', $middlewareClass);

			foreach( $classNameArray as $elem ) {
				$className .= ucfirst($elem);
			}

			$className .= 'Middleware';
			$middleWareFile = _APP . '/middleware/' . $middlewareClass . '.middleware.php';

			if( count( glob( $middleWareFile ) ) ) {
				require_once $middleWareFile;

				$oMiddleWare = new $className();

				if( !method_exists( $oMiddleWare, $middlewareMethod ) ) {
					throw new \ErrorsDeveloper( 'The method "' . $middlewareMethod . '" does not exist in class "' . $className . '"' );
				}

				if( $pAfter === false ) {
					$this->setMiddlewareResponse( $pType, $oMiddleWare->$middlewareMethod( $this->request ) );
				} else {
					$response = $oMiddleWare->$middlewareMethod( $pResponse );
				}
			} else {
				throw new \ErrorsDeveloper('There is not a middleware for this route (' . $pRouteMiddleware . ').');
			}
		} else {
			throw new \ErrorsDeveloper('Please, check that the middleware name has the correct nomenclature (' . $pRouteMiddleware . ').');
		}

		return $response;
	}

	public function run() {
		$response = $this->routesToResponse ? $this->routesToResponse : [];
		
		if( $this->routeFound || $this->options ) {
			$configFile = _CONFIG . '/modules/' . $this->module . '.config.php';
			$moduleConfig = [];
			$method = null;
			$methodBefore = null;

			//Agregar configuraciones del módulo.
			if( count( glob( $configFile ) ) ) {
				$moduleConfig = require $configFile;
				$this->lean->setConfig( array_merge( Helper::getConfig(), $moduleConfig ) );
			}

			$this->setCors( Helper::getConfig('cors') );

			if( $this->options ) {
				return;
			} else {
				if( count($this->routesFound) > 1 ) {
					$urisFound = '';

					for($x=0; $x<count($this->routesFound); $x++) {
						if($x > 0) {
							$urisFound .= ', ';
						}

						$urisFound .= '"' . $this->routesFound[$x]['uri'] . '"';
					}

					throw new \ErrorsRouter('There are more than one similar uri\'s for this route: ' . $urisFound);
				} else {
					$this->controller = $this->routesFound[0]['controller'];
					$this->method = $this->routesFound[0]['method'];
					$this->methodBefore = $this->routesFound[0]['methodBefore'];
					$this->verifyToken = $this->routesFound[0]['verifyToken'];
					$this->request->setParams( $this->routesFound[0]['requestParams'] );
					$this->request->setBaseURI( $this->routesFound[0]['requestBaseURI'] );
					$this->middlewareByUri = $this->routesFound[0]['middlewareByUri'];
					$this->middlewareByUriAfter = $this->routesFound[0]['middlewareByUriAfter'];
				}
			}
			
			$method = $this->method;
			$methodBefore = $this->methodBefore;
			$secretString = $this->lean->getConfig('secretString');
			$heanlderInfo = $this->getFileClassName($this->controller);
			$className = $heanlderInfo['className'];
			$oController = new $className;	
			
			if( $methodBefore ) $oController->$methodBefore();

			$this->getToken( false, $secretString );
			
			if( $this->middlewareGlobal ) $this->middleware( false, $this->middlewareGlobal, null, 'global' );
			if( $this->middlewareByVerb ) $this->middleware( false, $this->middlewareByVerb, null, 'verb' );
			if( $this->middlewareByUri ) $this->middleware( false, $this->middlewareByUri, null, 'uri' );

			$this->getToken( true, $secretString );
			
			$response = $oController->$method( $this->request );

			if( $this->middlewareByUriAfter ) $response = $this->middleware( true, $this->middlewareByUriAfter, $response, null );
			if( $this->middlewareByVerbAfter ) $response = $this->middleware( true, $this->middlewareByVerbAfter, $response, null );
			if( $this->middlewareGlobalAfter ) $response = $this->middleware( true, $this->middlewareGlobalAfter, $response, null );
		}

		return $response;
	}
}