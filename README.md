# lean php framework

## Requerimientos
* Apache/Nginx
* PHP
* Composer
* Mongo
* Driver PHP Mongo
* Clonar este repositorio a nivel raíz de tu proyecto cambiándole el nombre a lean (git clone https://gitlab.com/gilverduzco/lean-fw.git lean)

## URLs
* https://pecl.php.net/package/mongodb

## Esqueleto del proyecto
``` bash

app/
-----> modules/
----------> users/ # Ejemplo nombre de módulo
---------------> controllers/
--------------------> users.controller.php # Ejemplo nombre de controller
---------------> routes.config.php # Nombre de archivo de ruteo para las uri's
-----> domains/
-----> models/
-----> middleware/
config/
-----> modules/
----------> users.config.php # Ejemplo de configuración que aplica sólo a nivel módulo
-----> app.config.php # Archivo princial de configuración
-----> db-connections.config.php # Archivo principal para las conexiones a la base de datos
index.php
```

## Ejemplo app.config.php
``` bash

    # Se recomienda ignorar este archivo si tendrá información sensible o si la configuración es diferente por ambiente

<?php
    return [
        'siteName' => 'nombre-del-sitio',
        'environment' => 'production', # Este es el único ambiente que se valida, puedes tener cualquier ambiente con el nombre que quieras
        'debug' => false, # Mostrar detalle de los errores. No recomendado en el ambiente production
        'logger' => [ # Si no se desea loguear, cambiar este nombre o borrar
            'mongo' => [
                'host' => '127.0.0.1',
                'port' => 27017,
                'db' => 'LeanLogs'
            ]
        ]
    ];
?>
```

## Ejemplo db-connections.config.php
``` bash

# Este archivo debe ser ignorado por seguridad
# Puedes manejar "n" conexiones por ambiente

<?php
    /*
        dbType
            1. MySQL
            2. PostgreSQL
            3. SQLServer
    */

    return [
        'default' => 'conexion1',
        'dataSource' => [
            'development' => [
                'conexion1' => [
                    'host' => '127.0.0.1',
                    'port' => 5432,
                    'user' => 'username',
                    'password' => 'pass123#e',
                    'db' => 'dbname',
                    'dbType' => 'PostgreSQL',
                    'spPrefix' => 'p_' # Prefijo que deberán llevar los parámetros de los sp o funciones de las bases de datos
                ]
            ],
            'production' => [
                'conexion1' => [
                    'host' => '17.23.18.31',
                    'port' => 5432,
                    'user' => 'username',
                    'password' => 'pass123#e',
                    'db' => 'dbname',
                    'dbType' => 'PostgreSQL',
                    'spPrefix' => 'p_'
                ]
            ]
        ]
    ];
?>
```

## Ejemplo index.php

``` bash

# Este archivo va a nivel raíz de todo el proyecto

<?php
    require 'lean/autoload.php';
    require 'vendor/autoload.php';

    Lean::init()->run();
?>

```

## Contenido default archivo composer.json
``` bash

# Este archivo va a nivel raíz de todo el proyecto

{
    "require": {
        "firebase/php-jwt": "^5.0",
        "predis/predis": "^1.1"
    }
}
```

## Ejemplo .htaccess (Apache)
``` bash

# Este archivo va a nivel raíz de todo el proyecto

RewriteEngine On

# Some hosts may require you to use the `RewriteBase` directive.
# If you need to use the `RewriteBase` directive, it should be the
# absolute physical path to the directory that contains this htaccess file.
#
# RewriteBase /

RewriteRule .* index.php [QSA,L]

```