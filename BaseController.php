<?php
class BaseController {
    private $lean;

    public function __construct() {
        $this->lean = \Lean::getInstance();
    }

    public function getRouter() {
        return $this->lean->getRouter();
    }

    public function getHeaders( $pKey = null ) {
        return $this->lean->getRouter()->getRequest()->getHeaders( $pKey );
    }

    public function setHeader( $pKey, $pValue ) {
        return $this->lean->getRouter()->getRequest()->setHeader( $pKey, $pValue );
    }

    public function getBody( $pKey = null ) {
        return $this->lean->getRouter()->getRequest()->getBody( $pKey );
    }

    public function getQuery( $pKey = null ) {
        return $this->lean->getRouter()->getRequest()->getQuery( $pKey );
    }

    public function getParams( $pKey = null ) {
        return $this->lean->getRouter()->getRequest()->getParams( $pKey );
    }

    public function getToken() {
        return $this->lean->getRouter()->getRequest()->getToken();
    }

    public function getVerb() {
        return $this->lean->getRouter()->getRequest()->getVerb();
    }

    public function getTokenData( $pKey = null ) {
        return $this->lean->getRouter()->getRequest()->getTokenData( $pKey );
    }

    public function getMiddlewareResponse( $pKey = null ) {
        return $this->lean->getRouter()->getMiddlewareResponse($pKey);
    }

    public function setError( $pMessage, $pExtraValues = null ) {
        throw new \ErrorsController( $pMessage, $pExtraValues );
    }

    public function getDomain( $pRoute, $pDomain = null ) {
        return \Helper::getDomain($pRoute, $pDomain);
    }

    public function getModel( $pRoute, $pModel = null, $pIdCnn = null ) {
        return \Helper::getModel($pRoute, $pModel, $pIdCnn);
    }

    public function getConfig( $pKey = null ) {
        return \Helper::getConfig($pKey);
    }

    public function getEnv() {
        return $this->lean->getEnv();
    }    

    public function getModule() {
        return $this->lean->getRouter()->getModule();
    }

    public function checkPolicies( $pFileName, $pData, $pKey, $pExtraValues = null ) {
        $this->lean->checkPolicies( $pFileName, $pData, $pKey, $pExtraValues );
    }

    public function onFinish( $pFnc ) {
        $this->lean->onFinish( $pFnc );
    }
}